package com.happypeople.christmas.quiz2018

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

class Dec09Test {

    @Test
    fun dec09Test_star2() {
        runSim(411, 72059*100, 3615691746L)
    }

    @Test
    fun dec09Test() {
        runSim(411, 72059,429943 )
    }

    @Test
    fun run9_63() {
        runSim(9, 47, 63)
    }

    @Test
    fun run10_1618() {
        runSim(10, 1618, 8317)
    }

    @Test
    fun run17_1104() {
        runSim(17, 1104, 2764)
    }

    @Test
    fun run13_7999() {
        runSim(13, 7999, 146373)
    }

    @Test
    fun run21_6111() {
        runSim(21, 6111, 54718)
    }

    @Test
    fun run30_5807() {
        runSim(30,5807, 37305)
    }

    fun runSim(numPlayers: Int, maxMarble: Int, expectedHighScore: Long) {
        val sim=MarbleSim(numPlayers)
        sim.init()
        var fini=false
        var sCount=0
        while(!fini) {
            sCount++
            sim.step()
            /*
            if(lastWorth>0)
                println("after step $sCount worth: $lastWorth")
            println("step: $sCount marbles: ${sim.marbles}")
            */

            if(sCount==maxMarble)
                fini=true
        }

        val winner=sim.players.listOfValues().sortedByDescending { p -> p.score }.first()
        println("ran with numPlayers=$numPlayers, maxMarble=$maxMarble")
        println("winner ${winner.id} score: ${winner.score}")

        Assert.assertEquals("runSim p:$numPlayers steps:$maxMarble", expectedHighScore, winner.score)
    }

    class MarbleSim(val numPlayers: Int) {
        val marbles=Circle(0)
        val players=Circle(Player(1, 0))

        var nextMarble=1

        fun init() {
            for(i in 2..numPlayers)
                players.addValue(Player(i, 0))
        }

        fun step() {
            if((nextMarble%23)==0)
                step23()
            else
                stepNormal()

            // next Marble
            nextMarble+=1

            // next Player
            players.stepRight()
        }

        private fun step23() {
            var ret=nextMarble

            marbles.stepLeft7()
            ret+=marbles.removeCurrent()

            players.currentNode.value.score+=ret
        }

        private fun stepNormal() {
            marbles.stepRight()
            marbles.addValue(nextMarble)
        }
    }

    class LinkedNode<T>(val value: T) {
        var left =this
        var right =this
    }

    class Circle<T>(firstValue: T) {
        var currentNode: LinkedNode<T>

        init {
           currentNode=LinkedNode(firstValue)
        }

        override fun toString(): String {
            return "Circle: "+ listOfValues()
        }

        fun listOfValues(): List<T> {
            val ret= mutableListOf<T>()
            var lNode=currentNode
            do {
                ret.add(lNode.value)
                lNode=lNode.right
            }while(lNode!=currentNode)
            return ret
        }

        fun stepRight() {
            currentNode=currentNode.right
        }

        fun stepLeft7() {
            for(i in 1..7)
                currentNode=currentNode.left
        }

        fun addValue(pValue: T) {
            val newNode=LinkedNode(pValue)
            newNode.right=currentNode.right
            newNode.left=currentNode

            currentNode.right=newNode
            newNode.right.left=newNode

            currentNode=newNode
        }

        fun removeCurrent(): T {
            val ret=currentNode.value
            val lNode=currentNode.left
            val rNode=currentNode.right
            lNode.right=rNode
            rNode.left=lNode
            currentNode=rNode
            return ret
        }

    }

    class Player(val id: Int, var score: Long)

    @Test
    fun testAdd() {
        val list= mutableListOf<Int>(1, 2, 3)
        list.add(1, 42)
        Assert.assertEquals("third", 2, list[2])
        Assert.assertEquals("second", 42, list[1])

        list.add(4, 44)
        Assert.assertEquals("last", 44, list[4])
    }

    @Test
    fun testMod() {
        Assert.assertEquals("0", 0, 23%23)
        Assert.assertEquals("1", 1, 24%23)
        Assert.assertEquals("22", 22, 22%23)
    }

}
