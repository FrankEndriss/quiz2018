package com.happypeople.christmas.quiz2018

import org.junit.Assert
import org.junit.Test

// Idea from
// https://todd.ginsberg.com/post/advent-of-code/2018/day23/
// Thanks to Todd Ginsberg, great work!
/* Problem https://adventofcode.com/2018/day/23
 * In a group of Quadrants, find the point where most of them intersect.
 * Solved by Bron-Kerbosch algo.
 */
val par = SimpleParser()
val bots = mutableSetOf<NBot>()

fun manh(p1: P, p2: P): Long {
    return abs(p1.x - p2.x) + abs(p1.y - p2.y) + abs(p1.z - p2.z)
}

fun abs(long: Long): Long {
    if (long < 0)
        return -long
    else
        return long
}

data class P(val x: Long, val y: Long, val z: Long)

/** Defines a Quadrant by center and radius. */
data class NBot(val p: P, val r: Long) {
    fun intersect(other: NBot): Boolean {
        return r + other.r >= manh(p, other.p)
    }
}

class Dec23Test {

    fun parseBots() {
        bots.clear()
        for (line in par.resAsLines("/dec23.txt")) {
            val a = par.lineAsInts(line)
            bots.add(NBot(P(a[0].toLong(), a[1].toLong(), a[2].toLong()), a[3].toLong()))
            Assert.assertEquals("len 4", 4, a.size)
        }
    }

    @Test
    fun dec23_star1() {
        parseBots()
        val strongest = bots.sortedBy { -it.r }.first()
        val inRange = bots.filter { manh(strongest.p, it.p) <= strongest.r }
        val count = inRange.count()

        println("in range count: $count")
    }

    @Test
    fun dec23_star2() {
        parseBots()
        // create the graph
        val graph = bots.map { bot ->
            // note that in the graph the nodes must not be connected to themselves
            bot to (bots.filter { bot.intersect(it) }.minus(bot).toSet())
        }.toMap()

        val result = BronKerboschPivot(graph).run()
        val answer = result.map {
            println("res of size: ${it.size}")
            it.map { manh(it.p, P(0, 0, 0)) - it.r }.max()!!
        }.min()
        println("answer: $answer")
    }

    // see https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm
    // The variant without Pivot is incredible slow, did not finish
    // on my input data.
    // With pivot runs fast, less than two seconds
    class BronKerboschPivot<T>(val graph: Map<T, Set<T>>) {

        fun n(t: T): Set<T> {
            return graph[t]!!
        }

        /** analyze the graph */
        fun run(): Set<Set<T>> {
            val results = mutableSetOf<Set<T>>()
            execute(setOf<T>(), graph.keys.toSet(), setOf<T>(), results, 1)
            return results
        }

        private fun reportResult(results: MutableSet<Set<T>>, r: Set<T>) {
            if (results.isEmpty())
                results.add(r)
            else if (results.first().size < r.size) {
                results.clear()
                results.add(r)
            } else if (results.first().size == r.size)
                results.add(r)
        }

        private fun execute(r: Set<T>, p: Set<T>, x: Set<T>, results: MutableSet<Set<T>>, depth: Int) {
            // println("execute, sizes: ${r.size} ${p.size} ${x.size}, depth=$depth")
            if (p.isEmpty() && x.isEmpty()) {
                reportResult(results, r)
                // println("results.size: ${results.size}, depth=$depth")
            } else {
                val pivot = p.plus(x).maxBy { n(it).size }!!
                // val pivot = p.plus(x).first()
                p.minus(n(pivot)).forEach { v ->
                    execute(r.plus(v), p.intersect(n(v)), x.intersect(n(v)), results, depth + 1)
                }
            }
        }
    }
}