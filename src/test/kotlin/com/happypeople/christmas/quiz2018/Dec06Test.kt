package com.happypeople.christmas.quiz2018

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner


class Dec06Test {

    @Test
    fun testDec06() {
        var input: String = Dec01Test::class.java.getResource("/dec06.input").readText()
        val locations = parseInput(input)
        val maxX = locations.sortedByDescending { l -> l.p.x }.first().p.x
        val minX = locations.sortedBy { l -> l.p.x }.first().p.x
        val maxY = locations.sortedByDescending { l -> l.p.y }.first().p.y
        val minY = locations.sortedBy { l -> l.p.y }.first().p.y

        println("minX=$minX maxX=$maxX minY=$minY maxY=$maxY")

        // loop all points from minX/minY to maxX/maxY
        for (x in (minX..maxX)) {
            for (y in (minY..maxY)) {
                val p = Point(x, y)
                // println("checking point $p")
                // find min manhattan distance to loc, add p to that locations pointlist
                // if there is only exactly one nearest location
                val nearestLocs = locations.sortedBy { loc -> loc.manhattanDistance(p) }
                val firstLoc = nearestLocs[0]
                val secondLoc = nearestLocs[1]
                if (firstLoc.manhattanDistance(p) != secondLoc.manhattanDistance(p)) {
                    firstLoc.pointlist.add(p)
                } else {
                    // println("first and second same md, $firstLoc $secondLoc $p")
                }
            }
        }
        // locations.forEach { loc ->
        // println("Location: ${loc.p.x} ${loc.p.y}, pointlist.size=${loc.pointlist.size}")
        // }

        // find locations with no point on edge (since these are finite areas)
        // sort that locations by size of pointlist, descending
        val locsWOEdge = locations.filter { loc -> loc.pointlist.filter { p -> p.x == minX || p.x == maxX || p.y == minY || p.y == maxY }.isEmpty() }
        // locsWOEdge.forEach { loc ->
        // println("LocationWOEdge: ${loc.p.x} ${loc.p.y}, pointlist.size=${loc.pointlist.size}")
        // }

        val maxAreaLoc = locsWOEdge.sortedByDescending { loc -> loc.pointlist.size }.first()

        // first is the result of star 1
        println("maxArea loc: x=${maxAreaLoc.p.x} y=${maxAreaLoc.p.y}, size=${maxAreaLoc.pointlist.size}")


        // Star 2
        var counter=0
        for (x in (minX..maxX)) {
            for (y in (minY..maxY)) {
                val p = Point(x, y)
                val sumDist = locations.map { loc -> loc.manhattanDistance(p)}.sum()
                if(sumDist<10000)
                    counter++
            }
        }

        println("result star2 is: $counter")
    }

    @Test
    fun testSequences() {
        Assert.assertEquals("seq of 'to' should have len 2", 2, (1 to 10).toList().size)
        Assert.assertEquals("seq of '..' should have len 10", 10, (1..10).toList().size)
    }

    @Test
    fun testManhattan() {
        val point1=Point(1, 1)
        val point2=Point(22, 22)
        val point3=Point(333, 333)

        val loc1=Location(point1)
        val loc3=Location(point3)

        Assert.assertEquals("should be 42", 42, loc1.manhattanDistance(point2))
        Assert.assertEquals("should be 622", 622, loc3.manhattanDistance(point2))
    }

    fun parseInput(str: String): List<Location> {
        val list= mutableListOf<Location>()
        for (line in str.split("\n")) {
            val x=line.substring(0, line.indexOf(',')).toInt()
            val y=line.substring(line.indexOf(',')+2).toInt()
            list.add(Location(Point(x, y)))
        }
        return list
    }

    class Point(val x: Int, val y: Int)
    class Location(val p: Point) {
        val pointlist= mutableListOf<Point>()

        fun manhattanDistance(otherP: Point):Int {
            return ldiff(p.x, otherP.x) + ldiff(p.y, otherP.y)
        }

        private fun ldiff(i1: Int, i2: Int): Int {
            if(i2>i1)
                return i2-i1
            else
                return i1-i2
        }
    }
}
