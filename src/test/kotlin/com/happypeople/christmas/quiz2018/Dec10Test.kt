package com.happypeople.christmas.quiz2018

import org.junit.Test

class Dec10Test {
    var counter=0

    @Test
    fun testDec10() {
        var input: String = Dec01Test::class.java.getResource("/dec10.input").readText()

        println("parsing...")
        var points=parseInput(input)

        println("running...")
        for(i in 1..10008) {
            dumpPoints(points)
            points=points.map { p -> p.next() }
        }
    }

    private fun dumpPoints(points: List<Dec10Test.Point>) {
        // find if negative x exist
        val negCount=points.filter { p -> p.x<0 || p.y<0}.count()
        println("step: $counter negCount: $negCount")
        counter++

        val sorted=sortPoints(points)
        if(counter>10000) {
            val yMap=mutableMapOf<Long, MutableList<Point>>()
            for(p in sorted) {
                if(!yMap.containsKey(p.y))
                    yMap[p.y]= mutableListOf()

                yMap[p.y]!!.add(p)
            }

            var minY=sorted.minBy { p -> p.y }!!.y
            val maxY=sorted.maxBy { p -> p.y }!!.y
            var minX=sorted.minBy { p -> p.x }!!.x
            val maxX=sorted.maxBy { p -> p.x }!!.x

            var idx=minY
            while(idx<=maxY) {
                var line=""
                for(i in minX..maxX)
                    line=line+"."

                if(yMap.contains(idx)) {
                    val list= yMap[idx]!!
                    val arr=line.toCharArray()
                    for(p in list) {
                        arr[(p.x-minX).toInt()]='#'
                    }
                    line=String(arr)
                }
                idx++
                println(line)
            }

            println("next")

        }

    }

    private fun sortPoints(points: List<Dec10Test.Point>): List<Point> {
        return points.sortedBy { p -> p.y * 2000000000 + p.x }
    }


    fun parseInput(str: String): List<Point> {
        val ret= mutableListOf<Point>()

        for (line in str.split("\n")) {
            var lLine=line
            if(lLine.length<10)
                break
            //println("parsing line: $lLine")

            lLine=lLine.substring(lLine.indexOf('<')+1)
            if(lLine.startsWith(" "))
                lLine=lLine.substring(1)
            val x=lLine.substring(0, lLine.indexOf(',')).toLong()

            //println("parsing2 line: $lLine")
            lLine=lLine.substring(lLine.indexOf(',')+1)
            if(lLine.startsWith(" "))
                lLine=lLine.substring(1)
            if(lLine.startsWith(" "))
                lLine=lLine.substring(1)
            val y=lLine.substring(0, lLine.indexOf('>')).toLong()

            //println("parsing3 line: $lLine")
            lLine=lLine.substring(lLine.indexOf('<')+1)
            if(lLine.startsWith(" "))
                lLine=lLine.substring(1)
            val vX=lLine.substring(0, lLine.indexOf(',')).toLong()

            //println("parsing4 line: $lLine")
            lLine=lLine.substring(lLine.indexOf(',')+1)
            if(lLine.startsWith(" "))
                lLine=lLine.substring(1)
            if(lLine.startsWith(" "))
                lLine=lLine.substring(1)
            val vY=lLine.substring(0, lLine.indexOf('>')).toLong()

            ret.add(Point(x, y, vX, vY))
        }

        return ret
    }

    fun not(b: Boolean): Boolean {
        if(b)
            return false
        else
            return true
    }

    class Point(val x: Long, val y: Long, val vX: Long, val vY: Long) {
        fun next() : Point {
            return Point(x+vX, y+vY, vX, vY)
        }
    }
}