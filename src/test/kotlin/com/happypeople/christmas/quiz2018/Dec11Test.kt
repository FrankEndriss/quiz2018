package com.happypeople.christmas.quiz2018

import org.junit.Test

class Dec11Test {

	@Test
	fun dec11Test() {
		println("hello")

		val grid = mutableListOf<MutableList<Cell>>()
		for (i in 1..300) {
			grid.add(mutableListOf())
			for (j in 1..300)
				grid[i - 1].add(Cell(i, j))
		}

		val sMap = mutableListOf<KeyValue>()
		// note that this will get slower as s increases
		// but watching the result will show that the result is
		// at about 12... so we do not need to run to 299.
		for (s in 2..50) {
			val powerMap = mutableListOf<KeyValue>()
			for (x in 1..(300 - s)) {
				for (y in 1..(300 - s)) {
					val idx = "" + x + ":" + y
					powerMap.add(KeyValue(idx, blockPower(x - 1, y - 1, s, grid)))
				}
			}
			val kv = powerMap.sortedByDescending() { kv -> kv.v }.first()
			println("$s : ${kv.k} ${kv.v}")
			sMap.add(KeyValue(""+s, kv.v))
		}

		val kvSMap = sMap.sortedByDescending() { kv -> kv.v }.first()

		// println("sMap: $sMap")
		println("first: ${kvSMap.k} : ${kvSMap.v}")
	}

	class KeyValue(val k: String, val v: Int) {
		override fun toString(): String {
			return k + " - " + v
		}
	}

	fun blockPower(x: Int, y: Int, s: Int, grid: List<List<Cell>>): Int {
		var sum = 0
		for (dx in 0..(s-1))
			for (dy in 0..(s-1))
				sum = sum + grid[x + dx][y + dy].powerLevel()
		return sum
	}

	class Cell(val x: Int, val y: Int) {
		fun powerLevel(): Int {
			val rackId = x + 10
			val v = (rackId * y + 8868) * rackId

			// keep the hundred digit
			var h = v / 100;
			if (h < 100)
				h = 0
			else
				h = h - ((h / 10) * 10)
			return h - 5
		}
	}
}

