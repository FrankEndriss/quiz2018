package com.happypeople.christmas.quiz2018

import org.junit.Assert
import org.junit.Test


class SetTest {

    data class Point(val x:Int, val y:Int)

    class CPoint(var x:Int, var y:Int)

    /**
     * Note that we have to implement equals() and hashCode
     * to use CPoint2 as key in Set and Map.
     */
    open class CPoint2(val x:Int, var y:Int) {
        override fun equals(other: Any?): Boolean {
            return when(other) {
                is CPoint2 -> other.x==x && other.y==y
                else -> false
            }
        }

        override fun hashCode(): Int {
            return x*31+y
        }
    }

    class ExtPoint(x: Int, y:Int, var str: String) : CPoint2(x, y) {
        override fun toString() = str+": "+super.toString()
    }

    @Test
    fun testExtClass() {
        val set=mutableSetOf<CPoint2>()
        val map=mutableMapOf<CPoint2, String>()

        val p1=CPoint2(1, 1)
        val p2=CPoint2(2, 2)
        val p3=CPoint2(3, 3)
        val pe1=ExtPoint(1, 1, "blah")

        set.add(p1)
        set.add(p2)
        map[p1]=""+p1
        map[p2]=""+p2

        Assert.assertFalse("not contains p3", set.contains(p3))
        Assert.assertTrue("set contains CPoint2", set.contains(CPoint2(1, 1)))
        Assert.assertTrue("set contains ExtPoint", set.contains(pe1))

        Assert.assertFalse("set contains p1", map.contains(p3))
        Assert.assertTrue("map contains p1", map.contains(CPoint2(1, 1)))
        Assert.assertFalse("set contains ExtPoint", set.contains(ExtPoint(1, 2, "laber")))
    }

    @Test
    fun testDataClass2() {
        val set=mutableSetOf<CPoint2>()
        val map=mutableMapOf<CPoint2, String>()

        val p1=CPoint2(1, 1)
        val p2=CPoint2(2, 2)
        val p3=CPoint2(3, 3)

        set.add(p1)
        set.add(p2)
        map[p1]=""+p1
        map[p2]=""+p2

        Assert.assertFalse("not contains p3", set.contains(p3))
        Assert.assertTrue("set contains p1", set.contains(CPoint2(1, 1)))

        Assert.assertFalse("set contains p1", map.contains(p3))
        Assert.assertTrue("map contains p1", map.contains(CPoint2(1, 1)))
    }

    @Test
    fun testPlainClass() {
        val set=mutableSetOf<CPoint>()
        val map=mutableMapOf<CPoint, String>()

        val p1=CPoint(1, 1)
        val p2=CPoint(2, 2)
        val p3=CPoint(3, 3)

        set.add(p1)
        set.add(p2)
        map[p1]=""+p1
        map[p2]=""+p2

        Assert.assertFalse("not contains p3", set.contains(p3))
        Assert.assertTrue("set contains p1", set.contains(p1))
        Assert.assertFalse("set contains p1", set.contains(CPoint(1, 1)))

        Assert.assertFalse("set contains p1", map.contains(p3))
        Assert.assertFalse("map contains p1", map.contains(CPoint(1, 1)))
    }

    @Test
    fun testDataClass() {
        val set=mutableSetOf<Point>()
        val map=mutableMapOf<Point, String>()

        val p1=Point(1, 1)
        val p2=Point(2, 2)
        val p3=Point(3, 3)

        set.add(p1)
        set.add(p2)
        map[p1]=""+p1
        map[p2]=""+p2

        Assert.assertFalse("not contains p3", set.contains(p3))
        Assert.assertTrue("set contains p1", set.contains(Point(1, 1)))

        Assert.assertFalse("set contains p1", map.contains(p3))
        Assert.assertTrue("map contains p1", map.contains(Point(1, 1)))
    }
}

