package com.happypeople.christmas.quiz2018

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

class Dec04Test {

    val guards = mutableMapOf<String, Guard>()

    @Test
    fun mytest() {
        val fileContent: String = Dec01Test::class.java.getResource("/dec04.input").readText()

        val lines = mutableListOf<String>()
        for (line in fileContent.split("\n")) {
            lines.add(line)
        }
        lines.sort() // by date

        // Parser
        var currentGuard = Guard("pseudo")
        var sleepStart = -1
        var state = State.START
        for (line in lines) {
            if (state == State.START) {
                currentGuard = parseGuardBeginsShift(line)
                state = State.AWAKE
            } else if (state == State.AWAKE) {
                if (isTokenFallsAsleep(line)) {
                    sleepStart = parseMinutes(line)
                    state = State.SLEEPING
                } else { // token is next Guard
                    currentGuard = parseGuardBeginsShift(line)
                    state = State.AWAKE
                }
            } else if (state == State.SLEEPING) {
                if (isTokenWakesUp(line)) {
                    val wakeUp = parseMinutes(line)
                    val minutes = (sleepStart until wakeUp).toList()
                    currentGuard.dayRecords.add(DayRecord(parseDate(line), minutes))
                    state = State.AWAKE
                } else { // token is next Guard
                    val minutes = (sleepStart until 59).toList()
                    currentGuard.dayRecords.add(DayRecord(parseDate(line), minutes))
                    currentGuard = parseGuardBeginsShift(line)
                    state = State.AWAKE
                }
            }
        }

        /*
        for (g in guards.values) {
            val id = g.id
            val m = g.minutesAsleep()
            println("Guard: $id minutes: $m")
        }
        */

        // Star 1
        // Find the guard with the most sleeping minutes
        val mostSleepingGuard = guards.values.sortedWith(compareByDescending { g -> g.minutesAsleep() }).first()
        val id = mostSleepingGuard.id
        val minutes = mostSleepingGuard.minutesAsleep()
        println("Guard with most sleeping minutes: $id  minutes: $minutes")
        val mostAMinute=mostSleepingGuard.mostAsleepMinute()
        println("Most asleep minute of Guard $id is: $mostAMinute")

        // Star 2
        // find the most asleep minute for every guard,
        // then sort by number of times of that minute was asleep
        // and display first.
        val secondGuard=guards.values.sortedWith(compareByDescending { g -> g.sleepingTimesOfMinute(g.mostAsleepMinute()) }).first()
        val sGid=secondGuard.id
        val mostAMinute2 = secondGuard.mostAsleepMinute()
        println("Most asleep minunte of guard: $sGid, minute: $mostAMinute2")
    }

    private fun parseDate(line: String): String {
        return line.substring(1, 11)
    }

    private fun parseMinutes(line: String): Int {
        return line.substring(15, 17).toInt()
    }

    private fun isTokenWakesUp(line: String): Boolean {
        return line.contains("wakes up")
    }

    private fun isTokenFallsAsleep(line: String): Boolean {
        return line.contains("falls asleep")
    }

    private fun parseGuardBeginsShift(line: String): Dec04Test.Guard {
        if (!line.contains("Guard"))
            throw RuntimeException("not a Guard line: " + line)
        var gID = line.substring(25)
        gID = gID.substring(0, gID.indexOf(" "))
        return guards.getOrPut(gID) { Guard(gID) }
    }

    enum class State {
        START, // initial state, no current guard
        AWAKE, // current guard is awake
        SLEEPING // current guard is sleeping
    }

    class Guard(val id: String) {
        val dayRecords = mutableListOf<DayRecord>()

        fun minutesAsleep(): Int {
            return dayRecords.map { it -> it.minutesAsleep.size }.sum()
        }

        /** @return times this guard sleeped in that minute */
        fun sleepingTimesOfMinute(minute: Int): Int {
            return dayRecords.filter { rec -> rec.minutesAsleep.contains(minute)}.count()
        }

        fun mostAsleepMinute(): Int {
            // maps minute to times that minute guard was sleeping
            val myMap= mutableMapOf<Int, Int>()

            for(m in 0..59) {
                myMap[m]= myMap.getOrDefault(m, 0) // init map for case dayRecords is empty

                for (rec in dayRecords)
                    if (rec.minutesAsleep.contains(m))
                        myMap[m]= myMap.getOrDefault(m, 0)+1
            }
            return myMap.maxBy { (k, v) -> v }!!.key

        }
    }

    /**
     * @param date distinct string
     * @param minutesAsleep minuten in denen der Wächter geschlafen hat
     */
    class DayRecord(val date: String, val minutesAsleep: List<Int>)
}
