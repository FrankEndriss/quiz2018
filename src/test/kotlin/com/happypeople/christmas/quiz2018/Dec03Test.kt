package com.happypeople.christmas.quiz2018

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

class Dec03 {

    @Test
    fun countSquares() {
        val fileContent: String = Dec01Test::class.java.getResource("/dec03.claims").readText()

        val claims = mutableListOf<Rect>()
        for (line in fileContent.split("\n")) {
            // #5 @ 4,746: 29x14
            println(line)
            val idx0 = line.indexOf('#') + 1
            val id = line.substring(idx0, line.indexOf(' ')).toInt()
            val idx1 = line.indexOf('@') + 2
            val x = line.substring(idx1, line.indexOf(',')).toInt()
            val idx2 = line.indexOf(',') + 1
            val y = line.substring(idx2, line.indexOf(':')).toInt()
            val idx3 = line.indexOf(':') + 2
            val w = line.substring(idx3, line.indexOf('x')).toInt()
            val idx4 = line.indexOf('x') + 1
            val h = line.substring(idx4, line.length).toInt()

            println("adding Rect: $x $y $w $h")
            claims.add(Rect(id, x, y, w, h))
        }

        var maxX = 0
        var maxY = 0
        for (claim in claims) {
            if (maxX < claim.x + claim.w)
                maxX = claim.x + claim.w
            if (maxY < claim.y + claim.h)
                maxY = claim.y + claim.h
        }

        val squares2d = mutableListOf<MutableList<Int>>()
        var idx = 0
        while (idx < maxX) {
            squares2d.add(mutableListOf())
            var idy = 0
            while (idy < maxY) {
                squares2d[idx].add(0)
                idy = idy + 1
            }
            idx = idx + 1
        }

        for (claim in claims) {
            for (point in claim.points()) {
                squares2d[point.x][point.y] += 1
            }
        }

        var count = 0
        for (list in squares2d)
            for (i in list)
                if (i > 1)
                    count = count + 1

        println("num of squares on multiple claims: $count")

        for (claim in claims) {
            var match = true
            for (point in claim.points()) {
                if (squares2d[point.x][point.y] > 1)
                    match = false
            }
            if (match) {
                val id = claim.id
                println("id of matched claim: $id")
            }
        }

    }


    class Rect(val id: Int, val x: Int, val y: Int, val w: Int, val h: Int) {
        fun seq(i: Int, j: Int): List<Int> {
            val list = mutableListOf<Int>()
            var it = i
            while (it < j) {
                list.add(it)
                it++
            }
            return list
        }

        fun points(): List<Point> {
            val ret = mutableListOf<Point>()
            for (i in seq(x, x + w)) {
                for (j in seq(y, y + h)) {
                    ret.add(Point(i, j))
                }
            }
            return ret;
        }
    };

    class Point(var x: Int, var y: Int) {
        fun equals(p: Point): Boolean {
            return p.x == x && p.y == y;
        }

        override fun hashCode(): Int {
            return x * 31 + y;
        }
    };
}

