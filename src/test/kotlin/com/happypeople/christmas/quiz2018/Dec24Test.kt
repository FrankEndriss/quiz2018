package com.happypeople.christmas.quiz2018

import org.junit.Assert
import org.junit.Test
import kotlin.math.max
import kotlin.math.min

// global boost
var boost=0

class Dec24Test {
    // attack types
    enum class AType {
        radiation,
        bludgeoning,
        fire,
        slashing,
        cold
    }

    data class UType(val hit: Int, val att: Int, val type: AType, val initi: Int, val weaks: List<AType>, val immus: List<AType>) {
    }

    data class G(val uType: UType, var uCount: Int) {
        val id= gId++
        fun effPower(): Int {
            return max(0, uType.att * uCount)
        }

        // this group is hit with that power, units are killed
        // @return the count of killed units
        fun getHit(power:Int):Int {
            val killedUnits=power/uType.hit
            uCount-=killedUnits
            uCount=max(0, uCount)
            return killedUnits
        }


        // for display
        fun wouldKill(power: Int):Int {
            val killedUnits=power/uType.hit
            return min(killedUnits, uCount)
        }

        companion object {
            // use a id per group for nicer display
            var gId=1
        }
    }

    fun targetSelection(groups: List<G>, targets: List<G>): List<Pair<G, G?>> {
        val lTargets = targets.toMutableList()
        return groups.sortedWith(compareBy({ -it.effPower() }, { -it.uType.initi }))
                .map { group ->

                    val ret = Pair(group,
                            // lTargets.filter { it.wouldKill(demageSum(group, it))>0 }
                            lTargets.filter { demageSum(group, it)>0 }
                                    .sortedWith(compareBy({ -demageSum(group, it) }, { -it.effPower() }, { -it.uType.initi }))
                                    .firstOrNull()
                    )

                    if (ret.second != null)
                        Assert.assertTrue("remove found target", lTargets.remove(ret.second!!))

                    ret
                }
    }

    // calcs damage if target would be attacked by group
    private fun demageSum(group: G, target: G?): Int {
        if(target==null)
            return 0
        var demage=group.effPower()

        if (target.uType.immus.contains(group.uType.type))
            demage=0

        if(target.uType.weaks.contains(group.uType.type))
            demage*=2

        return demage
    }

    @Test
    fun dec24_star1() {
        parse("/dec24.txt")
        run()
        Assert.assertEquals("13331 units", 13331, infection.map { it.uCount }.sum())
    }

    @Test
    fun dec24_star2() {
        boost=0
        var fini=false
        while(!fini) {
            boost += 1

            parse("/dec24.txt")
            fini=run()
        }
        Assert.assertEquals("7476 units", 7476, immune.map { it.uCount }.sum())
    }

    @Test
    fun dec24_t() {
        boost=1570
        parse("/dec24_t.txt")

        run()
        Assert.assertEquals("51 units", 51, immune.map { it.uCount }.sum())
    }

    var infection=mutableListOf<G>()
    var immune=mutableListOf<G>()
    fun run():Boolean {

        var attacked=true // stop if no attacks any more, tie
        while(infection.size>0 && immune.size>0 && attacked) {
            attacked=false
            // println("*******************")
            // display1(immune, "Immune")
            // display1(infection, "Infection")

            listOf(
                    targetSelection(immune, infection),
                    targetSelection(infection, immune)
            ).flatten()
            .filter {
                it.second!=null
            }.sortedBy { // ...Groups attack in decreasing order of initiative
                -it.first.uType.initi
            }.forEach {
                // Hitting in order of attacking.
                // Note that the target will have a lesser effPower()
                // for attacking within this round if it is later in this list.
                val dSum=demageSum(it.first, it.second)
                // display2(it.first, it.second!!, dSum)
                if(it.second!!.getHit(dSum)>0)
                    attacked=true
            }

            // remove dead groups
            infection=infection.filter { it.uCount>0 }.toMutableList()
            immune=immune.filter { it.uCount>0 }.toMutableList()
        }

        // println("fini")
        val infU=infection.map { it.uCount }.sum()
        val immU=immune.map { it.uCount }.sum()
        // println("infU= $infU infection: $infection")
        // println("immU= $immU immune: $immune")
        return infection.isEmpty() // immu has won
    }

    //************************************************** debug
    private fun display2(gAtt:G, gTar:G, power: Int) {
        println("Group ${gAtt.id} attack Group ${gTar.id}, killing ${gTar.wouldKill(power)} units")
    }
    private fun display1(immune: MutableList<G>, str:String) {
         println("$str System:")
         immune.forEach {
             println("Group ${it.id} contains ${it.uCount} units")
         }
    }

    //************************************************** parser
    fun parse(res:String) {
        immune= mutableListOf<G>()
        infection= mutableListOf<G>()

        val par=SimpleParser()
        var inImmune=false
        var inInfection=false
        par.resAsLines(res).forEach {line ->
            println(line)
            if(line.length>5) { // skip empty lines
                if (line.startsWith("Immune")) {
                    inImmune = true
                    inInfection = false
                } else if (line.startsWith("Infection")) {
                    inImmune = false
                    inInfection = true
                } else {
                    val ints = par.lineAsInts(line)
                    val weaknesses = mutableListOf<AType>()
                    val immunities = mutableListOf<AType>()

                    listOf("weak to" to weaknesses, "immune to" to immunities).forEach {
                        if (line.contains(it.first)) {
                            val sub = line.substring(line.indexOf(it.first))
                            val words = par.lineAsWords(sub)
                            var i = 1
                            do {
                                i++
                                val t = words[i].substring(0, words[i].length - 1)
                                it.second.add(AType.valueOf(t))
                            } while (!(words[i].endsWith(";") || words[i].endsWith(")")))
                        }
                    }

                    val words = par.lineAsWords(line)
                    val atype = AType.valueOf(words[words.size - 5])
                    if (inImmune)
                        immune.add(G(UType(ints[1], ints[2]+boost, atype, ints[3], weaknesses, immunities), ints[0]))
                    else
                        infection.add(G(UType(ints[1], ints[2], atype, ints[3], weaknesses, immunities), ints[0]))
                }
            }
        }
    }

    //************************************************** data
    fun initImmune_T1() {
        immune= mutableListOf(
        G(UType(5390, 4507+boost, AType.fire, 2, listOf<AType>(AType.radiation, AType.bludgeoning), listOf<AType>()), 17),
        G(UType(1274, 25+boost, AType.slashing, 3, listOf<AType>(AType.bludgeoning, AType.slashing), listOf<AType>(AType.fire)), 989)
        )
    }
    fun initInfection_T1() {
        infection= mutableListOf(
        G(UType(4706, 116, AType.bludgeoning, 1, listOf<AType>(AType.radiation), listOf<AType>()), 801),
        G(UType(2961, 12, AType.slashing, 4, listOf<AType>(AType.fire, AType.cold), listOf<AType>(AType.radiation)), 4485)
        )
    }
}