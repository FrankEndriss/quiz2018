package com.happypeople.christmas.quiz2018

import org.junit.Test
import kotlin.math.abs

class Dec25Test {

    data class P(val x: Int, val y: Int, val z: Int, val t: Int) {
        fun manh(p: P): Int {
            return abs(x-p.x) + abs(y-p.y) + abs(z-p.z) + abs(t-p.t)
        }
        fun connected(p: P):Boolean {
            return manh(p)<=3
        }
    }

    @Test
    fun dec25_star1() {
        parse()
    }

    fun parse() {
        val par = SimpleParser()
        var points=par.resAsLines("/dec25.txt").map {
            val a = par.lineAsInts(it)
            P(a[0], a[1], a[2], a[3])
        }

        var i=0
        while(!points.isEmpty()) {
            val graph = points.map { p ->
                p to (points.filter { p.connected(it) }.toSet())
            }.toMap()

            var aGroup=setOf(points.first())
            var aCount=0
            while(aCount!=aGroup.size) {
                aCount=aGroup.count()
                aGroup=aGroup.map {
                    graph[it]!!
                }.flatten().toSet()
            }
            points=points.minus(aGroup)
            i++
            println("$i ${aGroup.size} $aGroup")
        }
        println("constCount $i")
    }
}