package com.happypeople.christmas.quiz2018

import org.junit.Test
import java.lang.RuntimeException

class Dec18Test {

    enum class Type {
        open,
        tree,
        lumber
    }

    data class Point(val x: Int, val y: Int) {
        fun pointsArround(): List<Point> {
            return mutableListOf(
                    Point(x-1, y-1),
                    Point(x-1, y),
                    Point(x-1, y+1),
                    Point(x, y-1),
                    Point(x, y+1),
                    Point(x+1, y-1),
                    Point(x+1, y),
                    Point(x+1, y+1)
            ).filter { it.x>=0 && it.y>=0 && it.x<=49 && it.y<=49 }
        }
    }

    var map = mutableMapOf<Point, Type>()
    val sizeX=50
    val sizeY=50

    @Test
    fun dec18_star1() {
        parseMap("/dec18.input")

        // maps results to maps
        val statelist=mutableMapOf<Int, MutableSet<MutableMap<Point, Type>>>()

        var loopfoundcount=0
        var state494: MutableMap<Point, Type>? = null
        var step=1
        while(step <= 1000000000) {
            var nextMap= mutableMapOf<Point, Type>()

            for(x in 0..sizeX-1) {
                for(y in 0..sizeY-1) {
                    val p=Point(x, y)
                    val t=map[p]
                    if(t==Type.open) {
                        if(p.pointsArround().filter { map[it]==Type.tree }.count() >=3)
                            nextMap[p]=Type.tree
                        else
                            nextMap[p]=Type.open
                    } else if(t==Type.tree) {
                        if(p.pointsArround().filter { map[it]==Type.lumber }.count() >=3)
                            nextMap[p]=Type.lumber
                        else
                            nextMap[p]=Type.tree

                    } else if(t==Type.lumber) {
                        if((p.pointsArround().filter { map[it]==Type.lumber }.count() >=1)
                            && (p.pointsArround().filter { map[it]==Type.tree }.count() >=1))
                            nextMap[p]=Type.lumber
                        else
                            nextMap[p]=Type.open
                    } else
                        throw RuntimeException("bad index $x $y")

                }
            }


            map=nextMap

            if(step==494) {
                step+=(28*35714254)
                state494 = map
            }

            val res=calcResult()
            val set=statelist[res]
            if(set==null)
                statelist[res]= mutableSetOf(map)
            else {
                if(set.contains(map)) {
                    println("map found, step=$step")
                    // println("494 found in step $step")
                    if(map==state494)
                        println("494 found, step=$step")

                    // if(loopfoundcount++==5)
                    //    break
                }
                set.add(map)
            }

            if(step%10000==0) {
                println("step $step res: $res")
            }
            if(step>=999999970) {
                println("step $step res $res")
            }
            step++
        }

        val trees=map.values.filter { it==Type.tree }.count()
        val lumbers= map.values.filter { it==Type.lumber }.count()
        println("result trees: $trees lumber: $lumbers sum ${trees*lumbers}")
    }

    fun calcResult(): Int {
        return map.values.filter { it==Type.tree }.count() *
            map.values.filter { it==Type.lumber }.count()
    }

    fun parseMap(name: String) {
        val par=SimpleParser()
        val lines=par.resAsLines(name)

        for(y in lines.indices) {
            val line=lines[y]
            for(x in line.indices) {
                if(line.toCharArray()[x]=='.')
                    map[Point(x, y)]=Type.open
                else if(line.toCharArray()[x]=='#')
                    map[Point(x, y)]=Type.lumber
                else if(line.toCharArray()[x]=='|')
                    map[Point(x, y)]=Type.tree
                else
                    throw RuntimeException("bad line: $line")
            }
        }

    }
}