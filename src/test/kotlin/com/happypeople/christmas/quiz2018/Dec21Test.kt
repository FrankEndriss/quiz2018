package com.happypeople.christmas.quiz2018

import org.junit.Test
import java.lang.RuntimeException

class Dec21Test {
    val par=SimpleParser()

    @Test
    fun dec21_star1() {
        val lines=par.resAsLines("/dec21.input")
        val instlist=mutableListOf<Instr>()

        val instrList= mutableListOf<Pair<Opcode, Instr>>()

        for(i in lines.indices) {
            val nums=par.lineAsInts(lines[i])
            val words=par.lineAsWords(lines[i])
            val opc=str2opcode(words[0])
            val instr= Instr(-1, nums[0], nums[1], nums[2])
            instrList.add(Pair(opc, instr))
        }

        val ipReg=4
        run(ipReg, instrList)
    }
    fun run(ipReg: Int, instrList: MutableList<Pair<Opcode, Instr>>) {

        var ip=0
        var count=0L

        // longjump 1
        // regs= mutableListOf(0, 10551383, 1, 2, 2, 4)
        // ip=4
        // val window=17726373000L
            val startvalue=0
            var regs = mutableListOf(startvalue, 0, 0, 0, 0, 0)
            var stopper = 10000000
            var instrCounter=0L
            var ip28CheckCount=0
            var loopSet=mutableSetOf<Int>()
            while (ip >= 0 && ip < instrList.size) {
            // while (ip >= 0 && ip < instrList.size && stopper-- != 0) {
                regs[ipReg] = ip

                //if(count>=window && count<window+100)
                //    println("ip=$ip regs=$regs ${instrList[ip].second.a} ${instrList[ip].second.b} ${instrList[ip].second.c} instr=${instrList[ip].first}")

                if(ip==28) {
                    println("ip=$ip regs=$regs count=${ip28CheckCount++}")
                    if (loopSet.contains(regs[1]) && regs[1]==0)
                        return
                    else
                        loopSet.add(regs[1])
                }
                instrList[ip].first.calc(regs, instrList[ip].second)
                instrCounter++
                ip = regs[ipReg]
                val str=opcode2str(instrList[ip].first.opcode)
                ip++

                // if((instrCounter%1000000L)==0L)
                //     println("${instrCounter/1000000L}M")
            }
            if(stopper==0)
                throw RuntimeException("solved: $startvalue")
            println("count=$count ip=$ip regs=$regs")
            println("instrcounter $instrCounter")

    }

    data class Instr(val opcode: Int, val a: Int, val b: Int, val c: Int)

    abstract class Opcode(val opcode: Int) {
        abstract fun calc(regs: MutableList<Int>, instr: Instr)
    }

    class OpAddr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a]+regs[instr.b]
        }
    }
    class OpAddi(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a]+instr.b
        }
    }

    class OpMulr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a]*regs[instr.b]
        }
    }
    class OpMuli(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a]*instr.b
        }
    }

    class OpBanr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a] and regs[instr.b]
        }
    }
    class OpBani(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a] and instr.b
        }
    }

    class OpBorr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a] or regs[instr.b]
        }
    }
    class OpBori(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a] or instr.b
        }
    }

    class OpSetr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a]
        }
    }
    class OpSeti(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=instr.a
        }
    }

    class OpGtir(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(instr.a>regs[instr.b]) 1 else 0
        }
    }
    class OpGtri(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(regs[instr.a]>instr.b) 1 else 0
        }
    }
    class OpGtrr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(regs[instr.a]>regs[instr.b]) 1 else 0
        }
    }

    class OpEqir(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(instr.a == regs[instr.b]) 1 else 0
        }
    }
    class OpEqri(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(regs[instr.a] == instr.b) 1 else 0
        }
    }
    class OpEqrr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(regs[instr.a] == regs[instr.b]) 1 else 0
        }
    }

    fun opcode2str(opc:Int):String {
        return listOf(
        "setr",
        "eqrr",
        "gtri",
        "muli",
        "eqir",
        "borr",
        "bori",
        "mulr",
        "gtrr",
        "seti",
        "banr",
        "eqri",
        "addr",
        "gtir",
        "addi",
        "bani"
        )[opc]
    }

    fun str2opcode(str: String): Opcode {
        return when(str) {
            "setr" -> opcodes[0]
            "eqrr" -> opcodes[1]
            "gtri" -> opcodes[2]
            "muli" -> opcodes[3]
            "eqir" -> opcodes[4]
            "borr" -> opcodes[5]
            "bori" -> opcodes[6]
            "mulr" -> opcodes[7]
            "gtrr" -> opcodes[8]
            "seti" -> opcodes[9]
            "banr" -> opcodes[10]
            "eqri" -> opcodes[11]
            "addr" -> opcodes[12]
            "gtir" -> opcodes[13]
            "addi" -> opcodes[14]
            "bani" -> opcodes[15]
            else -> throw RuntimeException("bad code: $str")
        }
    }

    val opcodes=listOf(
            OpSetr(0),
            OpEqrr(1),
            OpGtri(2),
            OpMuli(3),
            OpEqir(4),
            OpBorr(5),
            OpBori(6),
            OpMulr(7),
            OpGtrr(8),
            OpSeti(9),
            OpBanr(10),
            OpEqri(11),
            OpAddr(12),
            OpGtir(13),
            OpAddi(14),
            OpBani(15)
    ).sortedBy { opc -> opc.opcode }

}