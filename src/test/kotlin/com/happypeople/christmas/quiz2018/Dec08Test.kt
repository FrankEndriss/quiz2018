package com.happypeople.christmas.quiz2018

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

class Dec08Test {

    var nodeID=1

    @Test
    fun testDec08() {
        var input: String = Dec01Test::class.java.getResource("/dec08.input").readText()
        val rootNode= parseNode(Parser(input), "")
        println("sum meta1 of root node: ${rootNode.sumMetaStar1()}")
        println("sum meta2 of root node: ${rootNode.sumMetaStar2()}")
    }

	/**
	 * @param parser to parse more input
	 * @param parentPath for debug output
	 */
    fun parseNode(parser: Parser, parentPath: String): Node {
        val numChildren=parser.nextToken()
        val numMeta=parser.nextToken()
        val node=Node(nodeID)
        val lParentPath=parentPath+" "+nodeID
        nodeID++


        for(i in 1..numChildren)
            node.children.add(parseNode(parser, lParentPath))

        // println("finished children, will read $numMeta metadata entries")

        for(i in 1..numMeta) {
            // print("meta $i, input=${parser.str}")
            node.metadata.add(parser.nextToken())
        }

        // println("created node ${node.id} with $numChildren children and $numMeta metadata entries, parent=$parentPath meta=${node.metadata}")

        // println("finished node with $numChildren children and $numMeta metadata entries")
        return node
    }

    class Parser(var str: String) {
        fun nextToken(): Int {
            while(str.toCharArray()[0].isWhitespace())
                str=str.substring(1)
            var digitString=""
            while(str.toCharArray()[0].isDigit()) {
                digitString += str.substring(0, 1)
                str=str.substring(1)
            }
            return digitString.toInt()
        }
    }
    class Node(val id: Int) {
        val children= mutableListOf<Node>()
        val metadata= mutableListOf<Int>()

        fun sumMetaStar1(): Int {
            return children.map { child -> child.sumMetaStar1() }.sum() + metadata.sum()
        }

        fun sumMetaStar2(): Int {
            if(children.size==0) {
                val ret= metadata.sum()
                // println("star2 leaf-node id=$id, ret=$ret")
                return ret
            } else {
                // println("star2 non-leaf-node id=$id, children.size=${children.size} metadata.size=${metadata.size}")
                var ret=0
                for(idx in metadata) {
                    if(idx>0 && idx<=children.size)
                        ret+=children[idx - 1].sumMetaStar2()
                }

                /* this does not work for some reason :/
                val ret = metadata.map { idx ->
                    if (idx == 0 || idx > children.size - 1)
                        0
                    else
                        children[idx - 1].sumMetaStar2()
                }.sum()
                */
                // println("star2 non-leaf-node id=$id, ret=$ret")
                return ret
            }
        }
    }
}