package com.happypeople.christmas.quiz2018

import org.junit.Assert
import org.junit.Test


var file = ""
// global state
var walls = mutableMapOf<Point, Point>()
var elfs = mutableMapOf<Unit, Unit>()
var goblins = mutableMapOf<Unit, Unit>()

var sum = 0
var count = 0
var result = 0

fun isFreePoint(p: Point): Boolean =
        if (elfs.contains(p) || goblins.contains(p) || walls.contains(p))
            false
        else
            true

fun parseFrom(fileName: String) {
    file = fileName
    walls = parse('#') { x, y -> Point(x, y) }
    elfs = parse('E') { x, y -> Unit(x, y) }
    goblins = parse('G') { x, y -> Unit(x, y) }
}

fun <T> parse(pC: Char, creator: (x: Int, y: Int) -> T): MutableMap<T, T> {
    val map = mutableMapOf<T, T>()
    var input: String = Dec01Test::class.java.getResource(file).readText()
    var y = 0
    for (line in input.split("\n")) {
        if (line.length < 2)
            continue
        var x = 0
        for (c in line) {
            if (c == pC) {
                val t = creator(x, y)
                map[t] = t
            }
            x++
        }
        y++
    }
    return map
}

open class Point(var x: Int, var y: Int) : Comparable<Point> {

    override fun compareTo(other: Point): Int {
        if (y == other.y)
            return x - other.x
        return y - other.y
    }

    override fun hashCode(): Int {
        return x * 31 + y
    }

    override fun equals(other: Any?): Boolean {
        return when (other) {
            is Point -> other.x == this.x && other.y == this.y
            else -> false
        }
    }

    override fun toString(): String {
        return "P:$x:$y"
    }

    fun freePointsArround(): List<Point> {
        return pointsArround().filter { p -> isFreePoint(p) }
    }

    // all Poins arround in reading order
    fun pointsArround(): List<Point> {
        return listOf(
                Point(x, y - 1),
                Point(x - 1, y),
                Point(x + 1, y),
                Point(x, y + 1)
        )
    }
}

var gGlobalElvesAttackPower=3
fun initElfesAttackPower() {
    for(u in elfs.keys)
        u.power= gGlobalElvesAttackPower
}

/** In respect to hashCode and equals Unit is a Point,
 * and therefor interchangeable usable as key
 * in a map/set.
 */
class Unit(x: Int, y: Int) : Point(x, y) {
    var power = 3
    var hp = 200

    override fun toString(): String {
        return "U:$x:$y:$hp"
    }

    fun isGoblin(): Boolean {
        return goblins.contains(this)
    }

    // https://stackoverflow.com/questions/2311486/how-to-calculate-the-shortest-path-between-two-points-in-a-grid
    private fun findPathTo(target: Point): List<Point>? {
        // mapps all points in the grid to the abstand to this
        var distanceMap = mutableMapOf<Point, Int>()
        distanceMap[Point(this.x, this.y)] = 0
        var i = 0
        do {
            i++
            // list of all points next to the points marked with i
            // which are still not marked.
            val myList = distanceMap.filter { entry -> entry.value == i-1 }
                    .keys.map { point -> point.freePointsArround() }
                    .flatten()
                    .filter { point -> !(distanceMap.containsKey(point)) }
                    .map { p -> distanceMap[p] = i; i } // pseudo mapping, just put the points into the map

            // do this until target is marked, or no more not-marked points are found
        } while (myList.size > 0 && distanceMap[target] == null)

        // if target was not marked there is no path to target
        if (distanceMap[target] == null)
            return null

        // println("dm1, target=$target distanceMap: $distanceMap")

        // First remove all Point from distanceMap which are same distance as target, but are not target.
        // Then remove all Point of distance i-1 (recursive) which are not connected to at least on point with
        // distance i. That's a removal of all dead-ends.
        distanceMap.filter { entry -> entry.value == i }
                .filter { entry -> !(target == entry.key) }
                .map { entry -> distanceMap.remove(entry.key)  }

        // println("dm2, target=$target distanceMap: $distanceMap")

        while(i>0) {
            val possibleMarking = distanceMap
                    .filter { entry -> entry.value == i-1 }
                    .map { entry -> entry.key }

            // println("possible remove $i: $possibleMarking")

            for(p in possibleMarking) {
                // println("check $p, pointsArround=${p.pointsArround()}")
                if(p.pointsArround().filter { lp -> distanceMap.containsKey(lp) && distanceMap[lp]==i}.size == 0) {
                    // println("remove $p")
                    distanceMap.remove(p)
                }
            }
            i--
        }

        // Since we removed all dead-ends from distanceMap, we can now choose
        // any path from this to target marked with the distances.
        // They are all of equal, minimum length.
        // If there is more than one first step choose the one in reading order.

        // println("dm3, target=$target distanceMap: $distanceMap")
        val retList = mutableListOf(Point(this.x, this.y))
        i=1
        while(!retList.contains(target)) {
            retList.add(
                    distanceMap.filter { entry -> entry.value == i }
                            .keys
                            .sortedWith(Comparator<Point> { p1, p2 -> if (p1.y != p2.y) p1.y - p2.y else p1.x - p2.x })
                            .first())
            i++
        }

        return retList
    }

    fun pathTo(target: Point): List<Point>? {
        // println("from $this pathTo $target")
        if (target.x == this.x && target.y == this.y)
            return listOf(target)

        val ret = findPathTo(Point(target.x, target.y))
        // println("path is: $ret")
        return ret
    }

    fun getHitten(hit: Int) {
        hp = hp - hit
        if (hp <= 0) {
            goblins.remove(this)
            elfs.remove(this)
        }
    }

    fun moveTo(x: Int, y: Int) {
        val isG = isGoblin()
        goblins.remove(this)
        elfs.remove(this)
        this.x = x
        this.y = y
        if (isG)
            goblins[this] = this
        else
            elfs[this] = this
    }

    // the first unit arround this unit contained in s in reading order, or null if none
    // should be named findHitTarget
    fun unitArround(s: Map<Unit, Unit>): Unit? {
        val comp = Comparator<Unit?> { u1, u2 -> if (u1!!.hp != u2!!.hp) u1.hp - u2.hp else if (u1.y != u2.y) u1.y - u2.y else u1.x - u2.x }

        return pointsArround()
                .map { p -> s[p] }
                .filter { u -> u != null }
                .filter { u -> u!!.hp > 0 }
                .sortedWith(comp)
                .firstOrNull()
    }
}

class Dec15Test {
    @Test
    fun dec15_7Test() {
        parseFrom("/dec15_7.input")
        println("walls: $walls")
        run()
        Assert.assertEquals("not correct", "sum 937 count 20", "sum $sum count $count")
    }

    @Test
    fun dec15_6Test() {
        parseFrom("/dec15_6.input")
        println("walls: $walls")
        run()
        Assert.assertEquals("not correct", "sum 536 count 54", "sum $sum count $count")
    }

    @Test
    fun dec15_5Test() {
        parseFrom("/dec15_5.input")
        println("walls: $walls")
        run()
        Assert.assertEquals("not correct", "sum 793 count 35", "sum $sum count $count")
    }

    @Test
    fun dec15_4Test() {
        parseFrom("/dec15_4.input")
        println("walls: $walls")
        run()
        Assert.assertEquals("not correct", "sum 859 count 46", "sum $sum count $count")
    }

    @Test
    fun dec15_3Test() {
        // first of few examples
        parseFrom("/dec15_3.input")
        println("walls: $walls")
        run()
        Assert.assertEquals("not correct", "sum 982 count 37", "sum $sum count $count")
    }

    @Test
    fun dec15_2Test() {
        parseFrom("/dec15_2.input")
        println("walls: $walls")
        run()
        Assert.assertEquals("not correct", "sum 590 count 47", "sum $sum count $count")
    }

    @Test
    fun dec15Test() {
        parseFrom("/dec15.input")
        println("walls: $walls")
        run()
        Assert.assertEquals("not correct", "sum 2570 count 89", "sum $sum count $count")
    }

    @Test
    fun dec15_star2() {
        parseFrom("/dec15.input")
        val gElfsCount=elfs.size
        gGlobalElvesAttackPower = 3
        while (true) {
            parseFrom("/dec15.input")
            gGlobalElvesAttackPower += 1
            initElfesAttackPower()
            run()
            if(gElfsCount == elfs.size)
                break
        }

        Assert.assertEquals("not correct", "sum 1601 count 21", "sum $sum count $count")
    }


    fun run() {
        count = 0
        var fini = false
        while (!fini) {
            val unitList = listOf(elfs.values, goblins.values).flatten().sorted()
            // println("unitList: $unitList")
            for (u in unitList) {
                // println("**current unit $u")
                if (u.hp <= 0) // that one died
                    continue

                if ((u.isGoblin() && elfs.size == 0) || ((!u.isGoblin()) && goblins.size == 0)) {
                    fini = true
                    break
                }
                val enemies = if (u.isGoblin()) elfs else goblins
                val team = if (u.isGoblin()) goblins else elfs
                step(u, team, enemies)
            }
            if (fini)
                break

            count++
            println("*************************** after round: $count")
            println("Es: ${elfs.values.sorted()}")
            println("Gs: ${goblins.values.sorted()}")
            println("***************************")
        }

        println("*************************** after round: $count")
        println("Es: ${elfs.values.sorted()}")
        println("Gs: ${goblins.values.sorted()}")
        println("***************************")

        sum = elfs.values.sumBy { e -> e.hp } + goblins.values.sumBy { g -> g.hp }
        result = sum * count
        println("sum=$sum count=$count result=$result")
    }

    // return false if nothing to do
    fun step(unit: Unit, team: MutableMap<Unit, Unit>, enemies: MutableMap<Unit, Unit>) {
        findAndMoveToTarget(unit, enemies)
        val enemy = unit.unitArround(enemies)
        if (enemy != null) {
            enemy.getHitten(unit.power)
            // println("$unit attacked enemy=$enemy")
        }
    }

    fun comparePointsReadingOrder(p1: Point, p2: Point): Int {
        return if (p1.y == p2.y) p1.x - p2.x else p1.y - p2.y
    }

    fun findAndMoveToTarget(unit: Unit, enemies: Map<Unit, Unit>) {
        val nearEnemy = unit.unitArround(enemies)
        if (nearEnemy != null) // no move, we are there
            return

        // println("findAndMoveToTarget: unit=$unit enemies=${enemies.values}")
        val pathes = enemies.values.map { e -> e.freePointsArround() }
                .flatten().toSet()
                .map() { p -> unit.pathTo(p) }
                .sortedBy { path -> if (path != null) path.size else Int.MAX_VALUE }

        if (pathes.size == 0 || pathes[0] == null)
            ; // println("no path")
        else {
            val pathlen = pathes[0]!!.size
            val path = pathes.filter { path -> path != null && path.size == pathlen }
                    .sortedWith(Comparator<List<Point>?> { l1, l2 -> comparePointsReadingOrder(l1!![l1.size - 1], l2!![l2.size - 1]) })
                    .first()

            val stepTo = path?.get(1)
            if (stepTo != null) {
                // println("results in: $stepTo")
                unit.moveTo(stepTo.x, stepTo.y)
            }
        }

    }
}