package com.happypeople.christmas.quiz2018

import org.junit.Assert
import org.junit.Test
import java.util.*

class Dec20Test {
    val par=SimpleParser()

    data class P(val x: Int, val y: Int) {
        override fun toString(): String {
            return "P:"+x+":"+y
        }
    }

    interface Trans {
        fun trans(set:Set<P>):Set<P>
    }
    interface ComplexTrans:Trans {
        val childs:MutableList<Trans>
    }

    inner class ParallelTrans():ComplexTrans {
        override val childs= mutableListOf<Trans>()
        override fun trans(set: Set<P>): Set<P> {
            return childs.flatMap { child -> child.trans(set) }.toSet()
        }
    }

    inner class SequentialTrans():ComplexTrans {
        override val childs= mutableListOf<Trans>()
        override fun trans(set:Set<P>):Set<P> {
            var lSet=set
            for(chi in childs)
                lSet=chi.trans(lSet)
            return lSet
        }
    }

    val dirsMap=mutableMapOf(
            'N' to P(0, -1),
            'S' to P(0, 1),
            'W' to P(-1, 0),
            'E' to P(1, 0)
    )

    inner class SimpleTrans(val c:Char):Trans {

        override fun trans(set: Set<P>): Set<P> {
            val offs=dirsMap[c]!!
            return set.map { add2map(
                    P(it.x+2*offs.x, it.y+2*offs.y), '.',
                    P(it.x+offs.x, it.y+offs.y), 'D'
                ) }.toSet()
        }
    }

    fun add2map(p:P, c:Char, p2:P, c2:Char):P {
        map[p]=c
        map[p2]=c2
        return p
    }

    @Test
    fun dec20_star1() {
        regex=par.resAsString("/dec20.txt").trim()
        run()
    }

    // globals
    var regex=""
    val map= mutableMapOf<P, Char>()            // map of doors and places

    private fun run() {
        val stack=ArrayDeque<ComplexTrans>()    // stack of transitions
        stack.push(SequentialTrans())           // root group

        var idx=0
        while(idx<regex.length) {
            val c=regex[idx]
            if(c in listOf('N', 'S', 'W', 'E')) {
                stack.peek().childs.add(SimpleTrans(c))
            } else if(c=='(') { // start a new ParGroup and the first SeqGroup within that ParGroup
                stack.push(ParallelTrans())
                stack.push(SequentialTrans())
            } else if(c==')') { // end the current SeqGroup and pop the current ParGroup
                val finiSeqGroup=stack.pop()
                val finiParGroup=stack.pop()
                finiParGroup.childs.add(finiSeqGroup)
                stack.peek().childs.add(finiParGroup)
            } else if(c=='|') { // end the current SeqGroup and create a new one
                val finiSeqGroup=stack.pop()
                stack.peek().childs.add(finiSeqGroup)
                stack.push(SequentialTrans())
            } else
                println("ignored input: $c")

            idx++
        }

        Assert.assertEquals("groupStack.size==1", 1, stack.size)

        // create the map by traversing the tree of transitions
        val pointSet=stack.peek().trans(setOf(P(0, 0)))

        // println("map after stepping: $map")
        // vizMap()

        // only the rooms, no doors, no walls
        val allRooms=map.filter { it.value=='.' }.keys.toMutableSet()
        allRooms.remove(P(0,0))
        val distMap= mutableMapOf<P, Int>()

        // start at origin, mark all reachable point with distance,
        // repeat until all known points marked.
        distMap[P(0, 0)]=0
        var dist=0
        while(!allRooms.isEmpty()) {
            val nextPoints=distMap.filter { it.value==dist }
                    .flatMap { reachable(it.key) }
                    .filter {  !distMap.containsKey(it) } // only the still not marked ones

            for(p in nextPoints) {
                if(distMap[p]==null)
                    distMap[p]=dist+1
                allRooms.remove(p)
            }
            dist++

            if(nextPoints.isEmpty())
                  break
        }

        Assert.assertTrue("allPoints should be empty", allRooms.isEmpty())

        val maxDist=distMap.values.max()!!
        println("maxDist: $maxDist")
        val min1000=distMap.values.filter { it>=1000 }.count()
        println("min1000: $min1000")
    }

    fun vizMap() {
        val minX=map.keys.map { it.x }.min()!!
        val minY=map.keys.map { it.y }.min()!!
        val maxX=map.keys.map { it.x }.max()!!
        val maxY=map.keys.map { it.y }.max()!!

        println()
        for(y in minY-1..maxY+1) {
            for(x in minX-1..maxX+1) {
                val c=map[P(x, y)]
                if(x==0 && y==0)
                    print("X")
                else {
                    print(
                    if (c != null) {
                        if (c == 'D') {
                            if (y%2!=0)
                                '-'
                            else
                                '|'
                        } else
                            c
                    } else if (x % 2 != 0 && y % 2 != 0)
                        "#"
                    else
                        "?"
                    )
                }
            }
            println()
        }
    }

    // @return set of the points reachable by one step from p, may be empty
    // Basically this checks for all directions if there is a door, and if yes
    // steps throu it.
    fun reachable(p: P): List<P> {
        return dirsMap
                .filter { map[P(it.value.x+p.x, it.value.y+p.y)]=='D' }
                .map { P(it.value.x*2+p.x, it.value.y*2+p.y) }
                .toList()
    }
}