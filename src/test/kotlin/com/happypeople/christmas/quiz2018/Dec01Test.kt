package com.happypeople.christmas.quiz2018

import khttp.responses.Response
import org.json.JSONObject
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

class Dec01Test {

    @Test
    fun calcNumbers() {
        // val response: Response = khttp.get("https://adventofcode.com/2018/day/1/input");
        // print(response.text)
        // since this tells me i have to login, i use the hardcoded values

        val fileContent: String = Dec01Test::class.java.getResource("/dec01.numbers").readText()
        println("did read resource")
        // print(fileContent)
        val lines: List<String> = fileContent.split("\n")
        var result = 0
        for (line in lines) {
            val str = java.lang.String(line);
            result += Integer.parseInt(str.replace("\r", "").replace("\n", ""));
        }
        println("result: $result")
    }

    @Test
    fun reachTwice() {
        val fileContent: String = Dec01Test::class.java.getResource("/dec01.numbers").readText()
        print("did read resource")
        val lines: List<String> = fileContent.split("\n")
        var currentFreq = 0
        val freqs = mutableListOf<Int>()
        while(true) {
            for (line in lines) {
                val str = java.lang.String(line)
                val currentValue = Integer.parseInt(str.replace("\r", "").replace("\n", ""))
                currentFreq += currentValue
                if (freqs.contains(currentFreq)) {
                    println("freq reached twice: $currentFreq")
                    return
                }
                freqs.add(currentFreq)
                // println("currentFreq: $currentFreq")
            }
        }
    }

}