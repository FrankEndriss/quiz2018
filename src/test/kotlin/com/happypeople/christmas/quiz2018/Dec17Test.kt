package com.happypeople.christmas.quiz2018

import org.junit.Test
import java.util.regex.Pattern

class Dec17Test {

    data class Point(val x: Int, val y: Int)

    enum class Ent {
        clay,
        water,
        fixed,
    }

    val theMap=parseInput("/dec17_star1.input")

    @Test
    fun dec17_star1() {
        // val theMap=parseInput("/dec17_star1.input")
        val spring=Point(500, 0)
        val firstPoint=Point(spring.x, 1)
        theMap[firstPoint]=Ent.water
        val maxY=theMap.keys.map { p -> p.y }.max()!!

        // Naiv approach: change theMap until there is noting more to change.
        // Then collect the result.
        var changed=true
        var steps=0
        while(changed && steps<50000) {
            changed=false
            if(steps++%1000==0)
                println("step $steps")

            // add water below all waters where nothing is
            for(p in theMap.filter { kv -> kv.value==Ent.water }.keys) {
                if(p.y==maxY) // the world ends at maxY
                    continue

                val pBelow = Point(p.x, p.y + 1)
                if (theMap[pBelow] == null) {
                    theMap[pBelow] = Ent.water
                    changed = true
                }
            }

            // add water next to water if clay / fixed below
            for(p in theMap.filter { kv -> kv.value==Ent.water }.keys) {
                if(p.y==maxY) // the world ends at maxY
                    continue

                val tBelow = tBelow(p)
                if(tBelow==Ent.clay || tBelow==Ent.fixed) {
                    val pL=pLeft(p)
                    if(theMap[pL]==null) {
                        theMap[pL] = Ent.water
                        changed=true
                    }
                    val pR = pRight(p)
                    if(theMap[pR]==null) {
                        theMap[pR] = Ent.water
                        changed=true
                    }
                }
            }

            // find rows of water with fixed/clay on both ends, fix that rows
            for(p in theMap.filter { kv -> kv.value==Ent.water }.keys) {
                if(p.y==maxY) // the world ends at maxY
                    continue

                // check if on top of clay or fixed
                if(tBelow(p)==Ent.clay || tBelow(p)==Ent.fixed) {
                    // check if left end ends in clay/fixed
                    var pL= p
                    var leftEndFound=false
                    while((tBelow(pL)==Ent.clay || tBelow(pL)==Ent.fixed) && tLeft(pL)==Ent.water)
                        pL=pLeft(pL)
                    if( (tBelow(pL)==Ent.clay || tBelow(pL)==Ent.fixed) && (tLeft(pL)==Ent.clay || tLeft(pL)==Ent.fixed))
                        leftEndFound=true

                    // check if right end ends in clay/fixed
                    var pR= p
                    var rightEndFound=false
                    while((tBelow(pR)==Ent.clay || tBelow(pR)==Ent.fixed) && tRight(pR)==Ent.water)
                        pR=pRight(pR)
                    if( (tBelow(pR)==Ent.clay || tBelow(pR)==Ent.fixed) && (tRight(pR)==Ent.clay || tRight(pR)==Ent.fixed))
                        rightEndFound=true

                    if(leftEndFound && rightEndFound && theMap[p]==Ent.water) {
                        theMap[p] = Ent.fixed
                        changed=true
                    }
                }
            }
        }
        println("theMap.size=${theMap.size}")
        val result1=theMap.filter { ent -> (ent.value==Ent.water || ent.value==Ent.fixed) && ent.key.y<=maxY }.count()
        val result2=theMap.filter { ent -> (ent.value==Ent.fixed) && ent.key.y<=maxY }.count()
        println("result1: ${result1-2}")
        println("NOTE: -2")
        println("result2: $result2")
        vis(theMap)
    }

    fun pRight(p:Point): Point {
        return Point(p.x+1, p.y)
    }
    fun tRight(p:Point): Ent? {
        return theMap[pRight(p)]
    }
    fun pLeft(p:Point): Point {
        return Point(p.x-1, p.y)
    }
    fun tLeft(p:Point): Ent? {
        return theMap[pLeft(p)]
    }
    fun pBelow(p:Point): Point {
        return Point(p.x, p.y+1)
    }
    fun tBelow(p:Point): Ent? {
        return theMap[pBelow(p)]
    }

    private fun vis(theMap: Map<Point, Ent>) {
        val minX= theMap.keys.map() { p -> p.x }.min()!!
        val maxX= theMap.keys.map() { p -> p.x }.max()!!
        val maxY= theMap.keys.map() { p -> p.y }.max()!!

        for(y in 1..maxY) {
            println()
            for (x in minX..maxX) {
                val p = Point(x, y)
                if (theMap.contains(p)) {
                    val e = theMap[p]
                    if (e == Ent.clay)
                        print("#")
                    else if (e == Ent.fixed)
                        print("~")
                    else if (e == Ent.water)
                        print("|")
                }
                else
                    print(".")
            }
        }
        println()
    }

    private fun parseInput(f: String): MutableMap<Point, Ent> {
        var input: String = Dec01Test::class.java.getResource(f).readText()
        val lines = input.split("\n")
        val pDecimal= Pattern.compile("\\d+")
        val ret= mutableMapOf<Point, Ent>()

        var idx = 0
        while (idx < lines.size) {
            var line = lines[idx++]
            if(line==null || line.length<3)
                continue

            var m=pDecimal.matcher(line)
            if(line.startsWith("x")) {
                m.find()
                val x=m.group().toInt()
                m.find()
                val y1=m.group().toInt()
                m.find()
                val y2=m.group().toInt()
                for(y in y1..y2)
                    ret[Point(x, y)]=Ent.clay
            } else {
                m.find()
                val y = m.group().toInt()
                m.find()
                val x1 = m.group().toInt()
                m.find()
                val x2 = m.group().toInt()
                for(x in x1..x2)
                    ret[Point(x, y)]=Ent.clay
            }
        }
        return ret
    }

}