package com.happypeople.christmas.quiz2018

import org.junit.Test
import java.util.regex.Pattern

class ParserTest {

    @Test
    fun testParsers() {
        val pDecimal=Pattern.compile("\\d+")

        var input: String = Dec01Test::class.java.getResource("/dec16.input").readText()
        val lines = input.split("\n")

        var idx=0
        while(idx<lines.size) {
            var line=lines[idx]
            if(line.startsWith("Before")) {
                // println("line: $line")
                var m=pDecimal.matcher(line)
                println("Before: "+ listOf(1, 2, 3, 4).map { _ -> m.find(); m.group().toInt() })

                line=lines[++idx]
                // println("line: $line")
                m=pDecimal.matcher(line)
                println("Middle: "+ listOf(1, 2, 3, 4).map { _ -> m.find(); m.group().toInt() })

                line=lines[++idx]
                // println("line: $line")
                m=pDecimal.matcher(line)
                println("After: "+ listOf(1, 2, 3, 4).map { _ -> m.find(); m.group().toInt() })
            }
            idx++
        }

    }
}