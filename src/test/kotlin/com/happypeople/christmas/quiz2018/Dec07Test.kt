package com.happypeople.christmas.quiz2018

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.util.function.Consumer
import java.util.function.Predicate

class Dec07Test {

    @Test
    fun dec07Test() {
         var input: String = Dec01Test::class.java.getResource("/dec07.input").readText()

        val nodes=mutableListOf<Node>()
        nodes.addAll(parseInput(input))

        // first star
        var result=""
        while(!nodes.isEmpty()) {
            val node=findFirstNodeWithNoParent(nodes)
            result+=node?.value
            nodes.remove(node)
            for(lNode in nodes) {
                lNode.parents.remove(node)
            }
            println("result: $result")
        }

        ///////////////////////////////////////////
        // second star
        val freeWorkers=mutableListOf<Worker>()
        for(i in 1..5)
            freeWorkers.add(Worker(i))
        val workingWorkers=mutableListOf<Worker>()

        nodes.clear()
        nodes.addAll(parseInput(input))

        var second=-1    // one second per while loop
        var done=""
        while(!nodes.isEmpty() || !workingWorkers.isEmpty()) {
            second+=1
            var doneSomething=false
            val freeedWorkers= mutableListOf<Worker>()
            for(worker in workingWorkers.toList()) {
                worker.timeRemaining-=1
                if(worker.timeRemaining==0) { // that workers node is finished
                    done+=worker.worksOn?.value
                    nodes.remove(worker.worksOn)
                    for(node in nodes) {
                        node.parents.remove(worker.worksOn)
                    }
                    workingWorkers.remove(worker)
                    freeedWorkers.add(worker)
                }
                doneSomething=true
            }

            while(freeWorkers.size>0 && findFirstNodeWithNoParent(nodes)!=null) {
                doneSomething=true
                val node=findFirstNodeWithNoParent(nodes)
                val freeWorker=freeWorkers.removeAt(0)
                freeWorker.worksOn=node
                freeWorker.timeRemaining=timecalc(node!!.value)
                workingWorkers.add(freeWorker)
                nodes.remove(node)
            }

            freeWorkers.addAll(freeedWorkers)

            println("step $second done: $done")

            if(!doneSomething)
                throw RuntimeException("something went wrong")
        }
        println("second star needed $second seconds")

    }

    fun findFirstNodeWithNoParent(nodes: List<Node>): Node? {
        val list= nodes.filter { node -> node.parents.isEmpty() }
                .sortedBy { node -> node.value }

        return if(list.isEmpty()) null else list[0]
    }

    fun timecalc(c: Char):Int {
        return 61+(c-'A')
    }

    @Test
    fun testTimecalc() {
        Assert.assertEquals("D==64", 64, timecalc('D'))
    }

    @Test
    fun testFindFirst() {
        val nA=Node('A')
        val nB=Node('B')
        val nC=Node('C')
        nC.parents.add(nA)
        nB.parents.add(nC)
        Assert.assertEquals("nA has no parent", nA, findFirstNodeWithNoParent(listOf(nC, nB, nA)))
    }

    @Test
    fun textSeq() {
        Assert.assertEquals("a-z", 26, ('A'..'Z').count())
    }

    fun parseInput(str: String): MutableList<Node> {
        val map= mutableMapOf<Char, Node>()
        for (line in str.split("\n")) {
            if(line.length==48) {
                // println("${line.length}: $line")
                val parentC = line.toCharArray()[5]
                val childC = line.toCharArray()[36]
                if (!map.containsKey(parentC))
                    map[parentC] = Node(parentC)
                if (!map.containsKey(childC))
                    map[childC] = Node(childC)
                map[childC]?.parents?.add(map[parentC]!!)
            }
        }
        return map.values.toMutableList()
    }

    class Node(val value: Char) {
        val parents =mutableListOf<Node>()
    }

    class Worker(val id: Int) {
        var worksOn: Node? = null
        var timeRemaining = 0
    }

}