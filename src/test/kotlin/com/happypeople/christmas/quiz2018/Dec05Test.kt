package com.happypeople.christmas.quiz2018

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

class Dec05Test {

    @Test
    fun testDec05() {
        var input: String = Dec01Test::class.java.getResource("/dec05.input").readText()

        // star 1
        var str=reduce(input)
        var len=countUnits(str)
        println("final count of Units: $len")

        // star 2
        val aTOz="abcdefghijklmnopqrstuvwxyz"
        val pairs=aTOz.toList()
                .map { c -> Pair(c, countUnits(reduce(removeAll(c, input)))) }
                .sortedBy { pair -> pair.second }
		
        // for(p in pairs)
        //    println("Pair: ${p.first} ${p.second}")
        val pair=pairs.first()
        println("shortest reduce after removal, char: ${pair.first} len=${pair.second}")
    }

    fun removeAll(c: Char, pStr: String) : String {
        var str=pStr
        while(str.contains(c, true)) {
            str = str.replace(c.toLowerCase().toString(), "")
            str = str.replace(c.toUpperCase().toString(), "")
        }
        return str
    }

    fun reduce(pStr: String): String {
        var str=pStr

        // find index of first pair in str, then remove it.
        // loop with that until notfound
        var fini=false
        while(!fini) {
            // val s=str.substring(0, 80)
            // println("$s...")
            val idx=findPair(str)
            if(idx<0)
                fini=true
            else
                str=str.removeRange(idx, idx+2)
        }
        return str
    }

    fun findPair(str: String): Int {
        var idx=0
        var prevC=' '
        for(c in str) {
            if(     (c.isUpperCase() && c.toLowerCase().equals(prevC)) ||
                    (c.isLowerCase() && c.toUpperCase().equals(prevC))
            )
                return idx-1

            prevC=c
            idx+=1
        }
        return -1
    }

    fun countUnits(str: String): Int {
        var count=0
        for(c in str) {
            if(c.isLetter())
                count+=1
        }
        return count
    }
}