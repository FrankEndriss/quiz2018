package com.happypeople.christmas.quiz2018

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

class Dec02Test {

	fun isMatch(str1: String, str2: String): Boolean {
		val a1 = str1.toCharArray()
		val a2 = str2.toCharArray()
		var c = 0
		for (i in a1.indices) {
			if (a1[i] != a2[i]) {
				c++
			}
		}
		if (c == 1) {
			println()
			return true
		}
		return false
	}

	@Test
	fun secondStar() {
		val fileContent: String = Dec01Test::class.java.getResource("/dec02.lines").readText()

		for (line1 in fileContent.split("\n")) {
			val str1 = line1.replace("\r", "").replace("\n", "")
			for (line2 in fileContent.split("\n")) {
				val str2 = line2.replace("\r", "").replace("\n", "")
				if (isMatch(str1, str2)) {
					println("match:")
					println("$str1")
					println("$str2")
					return
				}
			}
		}
		Assert.fail("notfound")
	}

	@Test
	fun firstStar() {
		val fileContent: String = Dec01Test::class.java.getResource("/dec02.lines").readText()

		var zwei = 0
		var drei = 0

		for (line in fileContent.split("\n")) {
			val str = line.replace("\r", "").replace("\n", "")

			val map = mutableMapOf<Char, Int>()
			for (c in str) {
				map[c] = 1 + map.getOrPut(c) { 0 }
			}

			var incZwei = false
			for (p in map) {
				if (p.value == 2)
					incZwei = true
			}
			if (incZwei)
				zwei++

			var incDrei = false
			for (p in map) {
				if (p.value == 3)
					incDrei = true
			}
			if (incDrei)
				drei++
		}
		val checksum = zwei * drei
		println("checksum: $checksum")

	}

}