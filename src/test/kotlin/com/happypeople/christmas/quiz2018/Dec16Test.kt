package com.happypeople.christmas.quiz2018

import org.junit.Assert
import org.junit.Test
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.Unit

class Dec16Test {

    /*
    class Opc(name: String, op: (regs: MutableList<Int>, instr: Instr) -> Unit)

    val opcList=listOf(
            Opc("addr", { regs, instr ->
                regs[instr.c]=regs[instr.a]+regs[instr.b];
            }),
            Opc("addi", { regs, instr ->
                regs[instr.c]=regs[instr.a]+instr.b
            })
                    // ... more ops
    )

    enum class EOpc(op: (regs: MutableList<Int>, instr: Instr)-> Unit) {
        addr(
                { regs, instr ->
                    regs[instr.c]=regs[instr.a]+regs[instr.b]
                }),
        addi(
                { regs, instr ->
                    regs[instr.c]=regs[instr.a]+instr.b
                }),
        mulr(
                { regs, instr ->
                    regs[instr.c]=regs[instr.a]*regs[instr.b]
                })
        // ...more...
    }
    */

    data class Instr(val opcode: Int, val a: Int, val b: Int, val c: Int)

    abstract class Opcode(val opcode: Int) {
        abstract fun calc(regs: MutableList<Int>, instr: Instr)
    }

    class OpAddr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a]+regs[instr.b]
        }
    }
    class OpAddi(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a]+instr.b
        }
    }

    class OpMulr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a]*regs[instr.b]
        }
    }
    class OpMuli(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a]*instr.b
        }
    }

    class OpBanr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a] and regs[instr.b]
        }
    }
    class OpBani(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a] and instr.b
        }
    }

    class OpBorr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a] or regs[instr.b]
        }
    }
    class OpBori(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a] or instr.b
        }
    }

    class OpSetr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=regs[instr.a]
        }
    }
    class OpSeti(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c]=instr.a
        }
    }

    class OpGtir(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(instr.a>regs[instr.b]) 1 else 0
        }
    }
    class OpGtri(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(regs[instr.a]>instr.b) 1 else 0
        }
    }
    class OpGtrr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(regs[instr.a]>regs[instr.b]) 1 else 0
        }
    }

    class OpEqir(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(instr.a == regs[instr.b]) 1 else 0
        }
    }
    class OpEqri(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(regs[instr.a] == instr.b) 1 else 0
        }
    }
    class OpEqrr(opcode: Int) : Opcode(opcode) {
        override fun calc(regs: MutableList<Int>, instr: Instr) {
            regs[instr.c] = if(regs[instr.a] == regs[instr.b]) 1 else 0
        }
    }

    val opcodes=listOf(
            OpSetr(0),
            OpEqrr(1),
            OpGtri(2),
            OpMuli(3),
            OpEqir(4),
            OpBorr(5),
            OpBori(6),
            OpMulr(7),
            OpGtrr(8),
            OpSeti(9),
            OpBanr(10),
            OpEqri(11),
            OpAddr(12),
            OpGtir(13),
            OpAddi(14),
            OpBani(15)
    ).sortedBy { opc -> opc.opcode }

    data class Sample(val before: List<Int>, val after: List<Int>, val instr: Instr) {
        fun checkMatch(opcode: Opcode): Boolean {
            val mBefore= before.toMutableList()
            opcode.calc(mBefore, instr)
            // println("checking result=$before expected=$after for ${opcode.javaClass.simpleName}")
            return mBefore == after
        }

        fun listMatchedOpcodes(opcodes: List<Opcode>): List<Opcode> {
            return opcodes.filter { opc -> checkMatch(opc) }
        }

        override fun toString(): String {
            return "before: $before instr: $instr after: $after"
        }
    }

    @Test
    fun dec16Test() {
        run("/dec16.input")
    }

    @Test
    fun dec16_2Test() {
        run("/dec16_2.input")
    }


    private fun run(filename: String) {
        val samples=parseInput(filename)

        val map= mutableMapOf<Int, MutableList<Opcode>>()
        for(i in 0..15) {
            // add only the ones with still unknown opcode
            map[i]= opcodes.filter { opc -> opc.opcode<0 }.toMutableList()
        }

        var positiveSamples=0
        for(sample in samples) {
            val matches= opcodes.filter { opc -> sample.checkMatch(opc) }.count()

            println("sample: $sample matches: $matches")
            if(matches>=3)
                positiveSamples++


            // remove all opcodes from map which are not in matched Opcodes
            val maplist=map[sample.instr.opcode]!!
            val matchlist=sample.listMatchedOpcodes(opcodes)
            for(opc in opcodes) {
                if(!(matchlist.contains(opc)))
                    maplist.remove(opc)
            }
        }

        println("pos samples: $positiveSamples")
        /*
        for(i in 0..15) {
            println("opcodes: $i: ${map[i]}")
        }
        */

        // ************************* part2 run the programm
        val prog=parseInput2("/dec16_star2.input")
        val regs= mutableListOf(0, 0, 0, 0)
        for(instr in prog) {
            println("instr: $instr")
            opcodes[instr.opcode].calc(regs, instr)
        }
        println("regs after prog: $regs")
        Assert.assertEquals("result", 496, regs[0])
    }

    private fun parseInput2(filename: String): List<Instr> {
        val input = Dec01Test::class.java.getResource(filename).readText()

        val lines= input.split("\n")
        val pDec=Pattern.compile("\\d+")

        val ret= mutableListOf<Instr>()
        var lIdx =0
        while(lIdx<lines.size) {
            val line=lines[lIdx++]
            if(line.length>2) { // prevent empty lines
                val m=pDec.matcher(line)
                m.find()
                val v1 = m.group().toInt()
                m.find()
                val v2 = m.group().toInt()
                m.find()
                val v3 = m.group().toInt()
                m.find()
                val v4 = m.group().toInt()
                ret.add(Instr(v1, v2, v3, v4))
            }
        }
        return ret
    }

    private fun parseInput(filename: String): List<Sample> {
        val input: String = Dec01Test::class.java.getResource(filename).readText()
        val lines= input.split("\n")

        val pDec=Pattern.compile("\\d+")
        val retList= mutableListOf<Sample>()
        var lIdx=0
        while(lIdx<lines.size) {
            if(lines[lIdx].startsWith("Before")) {
                var m = pDec.matcher(lines[lIdx])
                val currBefore= generateSequence { if(m.find()) m.group().toInt() else null }.toList()

                m = pDec.matcher(lines[++lIdx])
                val currInstr= generateSequence { if(m.find()) m.group().toInt() else null }.toList()

                m = pDec.matcher(lines[++lIdx])
                val currAfter= generateSequence { if(m.find()) m.group().toInt() else null }.toList()

                retList.add(Sample(currBefore, currAfter, Instr(currInstr[0], currInstr[1], currInstr[2], currInstr[3])))
            }
            lIdx++
        }
        return retList
    }
}