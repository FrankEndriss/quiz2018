package com.happypeople.christmas.quiz2018

import org.junit.Test

class Dec12Test {

	val initialState =
			"##.####..####...#.####..##.#..##..#####.##.#..#...#.###.###....####.###...##..#...##.#.#...##.##.."
	val initLen=initialState.length

	@Test
	fun dec12Test() {

		var state = mutableSetOf<Int>()
		for (i in 0..initialState.length-1)
			if(initialState.toCharArray()[i].equals('#'))
				state.add(i)

		val rules = initRules()

		for (g in 1..50000000000) {
			val newState=mutableSetOf<Int>()
			var idxMin=state.min()!!-2
			var idxMax=state.max()!!+2

			for(idx in idxMin..idxMax) {
				var matchFound=false
				for (r in rules) {
					if(r.match(state, idx)) {
						if(matchFound)
							println("error, double match found: idx=$idx")
						if(r.ergebnis)
							newState.add(idx)
						matchFound=true
					}
				}
				if(!matchFound) {
					println("error, no match found: idx=$idx")
				}
			}
			state=newState

			var sum=state.sum()
			/*
			for (idx in state.min..state.max) {
				if(state.contains(idx)) {
					sum+=idx
				}
			}
			*/
			if(g==20L) {
				println("first star, sum after generation 20: $sum")
				val compSum=(50000000000L/100000)*32000*100 + 401
				println("second star: $compSum")
			}

			if(g%100000==0L) {
				println("sum after generation $g: $sum")
			}

			if(g==1000000L)
				return
            /*
			var stateAsString=""
			for (idx in 0..initialState.length - 1) {
				if(state.contains(idx))
					stateAsString=stateAsString+"#"
				else
					stateAsString=stateAsString+"."
			}
			println("state: "+stateAsString)
			*/
		}

	}

	class Rule(str: String) {
		val left2: Boolean
		val left1: Boolean
		val middl: Boolean
		val right1: Boolean
		val right2: Boolean
		public val ergebnis: Boolean

		init {
			this.left2 = str.toCharArray()[0].equals('#');
			this.left1 = str.toCharArray()[1].equals('#');
			this.middl = str.toCharArray()[2].equals('#');
			this.right1 = str.toCharArray()[3].equals('#');
			this.right2 = str.toCharArray()[4].equals('#');

			this.ergebnis = str.toCharArray()[9].equals('#');
		}
		
		/*
		fun step(state: Set<Int>): MutableSet<Int> {
			var idxMin=state.min()!!
			idxMin-=2
			var idxMax=state.max()!!
			idxMax+=2
			val ret=mutableSetOf<Int>()
			for (idx in idxMin..idxMax) {
				if(match(state, idx))
					ret.add(idx)
			}
			return ret
		}
 */

		fun match(state: Set<Int>, idx: Int): Boolean {
			return (state.contains(idx - 2) == this.left2) &&
				(state.contains(idx - 1) == this.left1) &&
				(state.contains(idx) == this.middl) &&
				(state.contains(idx + 1)== this.right1) &&
				(state.contains(idx + 2) == this.right2);
		}
	}
	private fun initRules(): List<Rule> {
		return listOf(
			"##.## => #",
			"....# => .",
			".#.#. => #",
			"..### => .",
			"##... => #",
			"##### => .",
			"###.# => #",
			".##.. => .",
			"..##. => .",
			"...## => #",
			"####. => .",
			"###.. => .",
			".#### => #",
			"#...# => #",
			"..... => .",
			"..#.. => .",
			"#..## => .",
			"#.#.# => #",
			".#.## => #",
			".###. => .",
			"##..# => .",
			".#... => #",
			".#..# => #",
			"...#. => .",
			"#.#.. => .",
			"#.... => .",
			"##.#. => .",
			"#.### => .",
			".##.# => .",
			"#..#. => #",
			"..#.# => .",
			"#.##. => #"
		).map() { s -> Rule(s) }
	}


}

