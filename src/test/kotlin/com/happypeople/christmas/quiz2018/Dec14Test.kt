package com.happypeople.christmas.quiz2018

import org.junit.Test

class Dec14Test {

    @Test
    fun dec14Test() {
        val input="37"
        // val prefixLen=2018
        // var input="084601"
        // val prefixLen=84601
        val pattern=listOf(0, 8, 4, 6, 0, 1)
        // val pattern=listOf(6, 7, 7, 9)
        // val pattern=listOf(5, 9, 4, 1, 4)
        // val pattern=listOf(9, 2, 5, 1, 0)
        // val pattern=listOf(0, 1, 2,4,5)

        val list=mutableListOf(3, 7)

        var elf1=0
        var elf2=1

        var pMatchIdx=0
        var checkAtIdx=0

        var step=0L
        while(true) {
            val appended=insertNewReceipes(list, elf1, elf2)
            // println("$elf1 $elf2 $list")
            // dumpList(startOfList, list, elf1, elf2)
            // println(simpleString(list.listOfValues()))

            while(checkAtIdx<list.size-pattern.size) {
                for(checkIdx in checkAtIdx..list.size-1) {
                    if (list[checkIdx] == pattern[pMatchIdx]) {
                        pMatchIdx++
                        if (pMatchIdx >= pattern.size) {
                            println("found pattern after recipes: ${checkAtIdx} ")
                            return
                        }
                    } else {
                        pMatchIdx = 0
                        break
                    }
                }
                checkAtIdx++
            }

            // step the elfes
            elf1=(elf1+list[elf1]+1)%list.size
            elf2=(elf2+list[elf2]+1)%list.size

            // output
            step+=appended
            if(step%1000000==0L)
                println("step $step")
        }


        /*
        print("result: ")
        for(i in prefixLen..prefixLen+9) {
            print("" + list[i])
        }
        println()
        */

    }

    fun simpleString(list: List<Int>): String {
        var ret=""
        for(i in list)
            ret=ret+i
        return ret
    }

    fun insertNewReceipes(list: MutableList<Int>, i1: Int, i2: Int): Int {
        val sum=list[i1]+list[i2]
        if(sum>9) {
            list.add(sum/10)
        }
        list.add(sum%10)

        return if(sum>9) 2 else 1
    }


}
