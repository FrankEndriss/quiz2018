package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

typealias NState = Int

class d22 {

    data class P(val x:Int, val y:Int) {
        fun plus(p:P):P {
            return P.of(x+p.x, y+p.y)
        }
        companion object {
            fun of(x:Int, y:Int):P {
                return P(x, y)
            }
        }
    }

    val clean=0
    val weakened=1
    val infected=2
    val flagged=3


    fun nextState(state:NState):NState {
        return (state+1)%4
    }

    val nodes= mutableMapOf<P, NState>()

    fun switchNodeInfected(p:P, currState:Int):Int {
        val next=nextState(currState)
        nodes.set(p, next)
        return if(next==infected) 1 else 0
    }

    fun switchNodeInfected1(p:P):Int {
        if(nodes.contains(p)) {
            nodes.remove(p)
            return 0
        } else {
            nodes[p]=infected
            return 1
        }
    }

    val north=0
    val dirs= listOf(
            P.of(0, -1),   // north/up
            P.of(1, 0),    // east/right
            P.of( 0, 1),   // ...
            P.of(-1, 0)
    )

    inner class VC(var pos:P, var dir:Int) {
        fun burst2():Int {
            var currState=nodes.getOrDefault(pos, clean)

            when(currState) {
                clean       -> dir=(dir+3)%4
                weakened    -> dir=dir // do nothing
                infected    -> dir=(dir+1)%4
                flagged     -> dir=(dir+2)%4
            }

            val ret= switchNodeInfected(pos, currState)
            pos=pos.plus(dirs[dir])
            return ret
        }

        fun burst():Int {
            dir=(dir + if(nodes.contains(pos)) 1 else 3 )%4

            val ret=switchNodeInfected1(pos)
            pos=pos.plus(dirs[dir])
            return ret
        }
    }


    @Test
    fun d22_star2() {
        val vc=parse()
        var c=0
        repeat(10000000) { c+=vc.burst2() }
        println("c: $c")
    }

    @Test
    fun d22_star1() {
        val vc=parse()
        var c=0
        repeat(10000) { c+=vc.burst() }
        println("c: $c")
    }

    fun parse():VC {
        var numLines=0
        var lenLines=0

        SimpleParser().resAsLines("/2017/in22.txt").mapIndexed { y, s ->
            lenLines=s.length
            numLines++
            s.mapIndexed { x, c ->
                if(c=='#')
                    nodes[P.of(x, y)]=infected
                1
            }
        }
        return VC(P.of(lenLines/2, numLines/2), north)
    }
}