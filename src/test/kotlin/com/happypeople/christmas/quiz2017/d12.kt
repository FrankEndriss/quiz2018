package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class d12 {
    val par=SimpleParser()

    @Test
    fun d12_star1() {
        val map= mutableMapOf<Int, MutableList<Int>>()
        for(line in par.resAsLines("/2017/in12.txt")) {
            val ints=par.lineAsInts(line)
            map[ints[0]]=ints.subList(1, ints.size)
        }


        val set2=mutableSetOf<MutableSet<Int>>()
        for(group in map.keys.min()!!..map.keys.max()!!) {
            var size=0
            var set=mutableSetOf(group)
            while (size != set.size) {
                size = set.size
                set.addAll(set.flatMap() {
                    map.getOrDefault(it, mutableSetOf<Int>())
                })
            }
            if(group==0)
                println("star1 groupsize: $size")

            if(set.size>0)
                set2.add(set)
        }
        // remove all groups from set where another group exists containing all
        // entries of this group
        val setFiltered= mutableSetOf<MutableSet<Int>>()
        for(set3 in set2) {
            var contained=false
            for(set4 in set2) {
                if (set4.size > set3.size && set4.containsAll(set3))
                    contained=true
            }
            if(!contained)
                setFiltered.add(set3)
        }
        println("star2 groupcount: ${setFiltered.size}")
    }
}