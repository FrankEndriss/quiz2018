package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test
import java.lang.RuntimeException

class d09 {

    var sum=0
    var gCount=0
    var line="".toCharArray()

    @Test
    fun d09_star1() {
        val par=SimpleParser()
        line=par.resAsString("/2017/in09.txt").toCharArray()

        parseGroup(0, 1)
        println("sum: $sum gCount: $gCount")
    }

    fun parseGroup(i: Int, groupValue: Int): Int {
        if(line[i]!='{')
            throw RuntimeException("wrong parser")

        sum+=groupValue
        println("sum $sum incremented by $groupValue")

        var idx=i
        while(line[idx]!='}') {
            idx++
            if(line[idx]=='<') // garbage
                idx=parseUntilEndOfGarbage(idx)
            else if(line[idx]=='{')
                idx=parseGroup(idx, groupValue+1)
        }
        return idx+1
    }

    fun parseUntilEndOfGarbage(i: Int):Int {
        if(line[i]!='<')
            throw RuntimeException("wrong parser in garbage")

        var idx=i+1
        while(line[idx]!='>') {
            if(line[idx]=='!')
                idx++
            else
                gCount++
            idx++
        }
        return idx
    }
}