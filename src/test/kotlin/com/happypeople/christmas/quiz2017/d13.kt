package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class d13 {
    val par=SimpleParser()

    val walls= mutableMapOf<Int, Int>()

    @Test
    fun d13_star1() {
        for(line in par.resAsLines("/2017/in13.txt")) {
            val ints = par.lineAsInts(line)
            walls[ints[0]]=ints[1]
        }
        // println("$walls")

        var sum=-1
        var offset=-1
        var cought=true
        while(cought)  {
            offset++
            cought=false
            sum=0
            for(i in 0..walls.keys.max()!!) {
                if(isPos0atStep(i, i+offset)) {
                    sum += i * walls[i]!!
                    cought=true
                }
            }
            if(offset==0)
                println("sum star1: $sum")
        }
        println("offset star2: $offset")

    }

    fun isPos0atStep(layer: Int, step: Int):Boolean {
        if(walls[layer]!=null) {
            val depth=walls[layer]!!
            val periode=(depth-1)*2
            return (step%periode)==0
        }
        return false
    }
}