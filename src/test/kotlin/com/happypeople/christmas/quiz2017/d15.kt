package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Assert
import org.junit.Test

var seedA=0L
var seedB=0L

val genA= generateSequence {
    seedA=(seedA*16807)%2147483647L
    seedA
}

val gen2A= generateSequence {
    do {
        seedA=(seedA*16807)%2147483647L
    }while(seedA%4!=0L)
    seedA
}

val genB= generateSequence {
    seedB=(seedB*48271)%2147483647L
    seedB
}

val gen2B= generateSequence {
    do {
        seedB=(seedB*48271)%2147483647L
    }while(seedB%8!=0L)
    seedB
}

val genP= genA.zip(genB)
val gen2P= gen2A.zip(gen2B)

class d15 {
    @Test
    fun d15_star1() {
        parse()
        val x=genP.take(40000000).filter {
            it.first and 0xFFFF == it.second and 0xFFFF
        }.count()
        println("result: $x")
    }

    @Test
    fun d15_star2() {
        parse()
        val x=gen2P.take(5000000).filter {
            it.first and 0xFFFF == it.second and 0xFFFF
        }.count()
        println("result: $x")
    }

    @Test
    fun testParse() {
        parse()
        Assert.assertEquals(883, seedA)
        Assert.assertEquals(879, seedB)
    }

    fun parse()  {
        val par=SimpleParser()
        par.resAsLines("/2017/in15.txt").forEach {
            if(it.contains(" A "))
                seedA=par.lineAsInts(it)[0].toLong()
            else if(it.contains(" B "))
                seedB=par.lineAsInts(it)[0].toLong()
        }
    }
}