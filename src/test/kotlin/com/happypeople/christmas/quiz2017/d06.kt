package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class d06 {
    @Test
    fun do6_both() {
        run()
    }

    fun run() {
        val par=SimpleParser()
        val banks=par.lineAsInts(par.resAsString("/2017/in06.txt"))
        println("initial banks: $banks")
        val map=mutableMapOf<String, Int>()
        var count=0
        while(!map.contains("$banks")) {
            map["$banks"]=count
            var maxBlocks=banks.max()!!
            var idx=-1
            for(i in banks.indices)
                if(banks[i]==maxBlocks) {
                    idx=i
                    break
                }
            // banks[idx] has max blocks (first of ties)
            banks[idx]=0
            while(maxBlocks>0) {
                idx=(idx+1)%banks.size
                banks[idx]++
                maxBlocks--
            }
            count++
        }
        println("result: $count")
        val from =map["$banks"]!!
        println("from step: $from")
        val diff=count-from
        println("diff is: $diff")
    }

}