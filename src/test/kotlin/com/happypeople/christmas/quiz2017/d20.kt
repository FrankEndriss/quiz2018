package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test
import kotlin.math.abs
import kotlin.math.min

// 1234567890-=
// !@#$%^&*()_+

// \zxcvbnm,./
// |ZXCVBNM<>?
class d20 {

    data class P(val x: Int, val y: Int, val z: Int) {
        fun manh(p: P): Int {
            return abs(x - p.x) + abs(y - p.y) + abs(z - p.z)
        }
    }

    data class Par(val p: P, val v: P, val a: P) {
        fun tick(): Par {
            val v2 = P(v.x + a.x, v.y + a.y, v.z + a.z)
            val p2 = P(p.x + v2.x, p.y + v2.y, p.z + v2.z)
            return Par(p2, v2, a)
        }
    }

    @Test
    fun d20_star1() {
        val origin = P(0, 0, 0)
        val res = parse().mapIndexed { idx, par ->
            Pair(idx, par)
        }.sortedWith(compareBy({ it.second.a.manh(origin) }, { it.second.p.manh(origin) })).first().first
        println("res: $res")
    }

    @Test
    fun d20_star2() {
        val origin = P(0, 0, 0)
        var list = parse().toMutableList()

        var minDist = 1000000000
        var oldMinDist = minDist + 1
        var minCount = 1000
        // while (oldMinDist >= minDist || minCount-- > 0) {
        while (minCount-- > 0) {
            oldMinDist = minDist
            minDist = 1000000000
            list = list.map { it.tick() }.toMutableList()

            val dist = list.map { par ->
                list.filter { it != par }
                        .map { Pair(par, it.p.manh(par.p)) }
                        .sortedBy { it.second }
                        .first()
            }

            list = dist.map {
                minDist = min(minDist, it.second)
                it
            }.filter {
                it.second > 0
            }.map { it.first }
                    .toMutableList()
            // println("min=$minDist, oldMin=$oldMinDist list.size=${list.size}")
        }
        println("min=$minDist, oldMin=$oldMinDist list.size=${list.size}")
    }


    fun parse(): List<Par> {
        val par = SimpleParser()
        return par.resAsLines("/2017/in20.txt").map { line ->
            val i = par.lineAsInts(line)
            Par(
                    P(i[0], i[1], i[2]),
                    P(i[3], i[4], i[5]),
                    P(i[6], i[7], i[8])
            )
        }
    }
}