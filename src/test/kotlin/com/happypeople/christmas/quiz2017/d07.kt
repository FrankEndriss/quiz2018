package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class Node(val name: String, var w: Int) {
    val childs = mutableListOf<Node>()

    fun weight(): Int {
        return childs.map { it.weight() }.sum()+w
    }
}

val map=mutableMapOf<String, Node>()

class d07 {

    @Test
    fun d07_star1() {
        run()
    }

    fun parentOf(name: String): Node? {
        return map.filter { ent -> ent.value.childs.filter { it.name==name }.size>0 }.values.firstOrNull()
    }

    fun getCreateNode(name: String, w: Int): Node {
        if(name.trim().length==0)
            throw RuntimeException("empty name")
        if(map.contains(name)) {
            val node=map[name]!!
            if(w>0)
                node.w=w
            return node
        }

        val node=Node(name, w)
        map[name]=node
        return node
    }

    fun run() {
        val par=SimpleParser()
        for(line in par.resAsLines("/2017/in07.txt")) {
            val words=par.lineAsWords(line).map { it.replace(",", "") }
            println("$line  || size ${words.size} words: $words")
            val w1=par.lineAsInts(words[1]).first()

            val parent=getCreateNode(words[0], w1)

            var i=3
            while(i<words.size) {
                val cnode=getCreateNode(words[i], -1)
                parent.childs.add(cnode)
                i++
            }
        }
        var node=map.values.firstOrNull()
        while(parentOf(node!!.name)!=null)
            node=parentOf(node!!.name)

        println("root node: $node name=${node.name}")

        printW(node, "")
    }

    fun printW(node: Node, prefix: String) {
        // println("$prefix ${node.name}:${node.weight()}")
        if(node.childs.map { it.weight() }.distinct().count()>1) {
            println("unbalanced childs: ${node.name}")
            for(child in node.childs)
                println("$prefix childs: ${child.name} ${child.weight()} ${child.w}")
        }

        for(child in node.childs)
            printW(child, prefix+"-->")
    }
}