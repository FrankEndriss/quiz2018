package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class d10 {
    val par=SimpleParser()

    @Test
    fun d10_star1() {
        val list=(0..255).toMutableList()
        var currentPos=0
        var input=par.lineAsInts(par.resAsString("/2017/in10.txt"))
        var inIdx=0
        var skipSize=0

        for(inIdx in input.indices) {
            val inLen=input[inIdx]

            replace(list, currentPos, selectSublist(inLen, currentPos, list))
            currentPos=(currentPos+inLen+skipSize)%list.size
            skipSize++
        }

        val res=list[0]*list[1]
        println("res $res")
    }

    @Test
    fun d10_star2() {
        val list=(0..255).toMutableList()
        var input=par.lineAsAscii(par.resAsString("/2017/in10.txt").trim())
        input.addAll(listOf(17, 31, 73, 47, 23))

        var currentPos=0
        var skipSize=0

        for(r in 1..64) {
            for (inIdx in input.indices) {
                val inLen = input[inIdx]
                replace(list, currentPos, selectSublist(inLen, currentPos, list))
                currentPos = (currentPos + inLen + skipSize) % list.size
                skipSize++
            }
        }

        // 16 numbers
        val denseHash=sparse2dense(list)
        val res=denseHash.map { it.toString(16) }
                .map { if(it.length<2) "0"+it else it }
                .joinToString("")

        println("res $res")
    }

    private fun sparse2dense(list: MutableList<Int>): MutableList<Int> {
        var subIdx=0
        val denseHash= mutableListOf<Int>()
        while(subIdx<list.size) {
            var h=list[subIdx]
            for(i in subIdx+1..subIdx+15)
                h = h xor list[i]
            denseHash.add(h)
            subIdx+=16
        }
        return denseHash
    }

    private fun replace(list: MutableList<Int>, currentPos: Int, sublist: MutableList<Int>) {
        for(i in sublist.indices)
            list[(currentPos+i)%list.size]=sublist[i]
    }

    private fun selectSublist(inLen: Int, pCurrentPos: Int, list: MutableList<Int>): MutableList<Int> {
        val ret= mutableListOf<Int>()
        var currentPos=pCurrentPos
        for(i in 1..inLen) {
            ret.add(list[currentPos])
            currentPos=(currentPos+1)%list.size
        }
        ret.reverse()
        return ret
    }
}