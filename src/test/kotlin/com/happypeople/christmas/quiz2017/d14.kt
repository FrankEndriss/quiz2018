package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class d14 {
    val par= SimpleParser()

    data class Point(val x: Int, val y: Int) {
        fun pointsArround(): List<Point> {
            return listOf(
                    Point(x-1, y),
                    Point(x, y+1),
                    Point(x+1, y),
                    Point(x, y-1)
            )
        }
    }


    val list= mutableListOf<Int>()
    @Test
    fun d14_star1() {

        var bitcount=0
        val pointSet= mutableSetOf<Point>()
        for(i in 0..127) {
            val list=(0..255).toMutableList()
            val str="hwlqcszp-"+i
            //val str="flqrgnkx-"+i
            val hex=knotHashAsHex(str, list)

            // println("$hex ${hex.length} $str")
            bitcount+=hex.map { countBits(it) }.sum()
            val pic= mutableListOf("")
            hex.map { pic[0]=pic[0]+printBits(it); 1 }.toList()
            pic[0].mapIndexed({ col, c ->  if(c=='#') pointSet.add(Point(col, i)); 1}).sum()
        }
        println("bitcount: $bitcount")
        // println("pointSet: $pointSet")

        var count=0
        while(!pointSet.isEmpty()) {
            count++
            // start at any point, remove all adjacent points
            var p=pointSet.first()
            pointSet.remove(p)
            var pNext=p.pointsArround().filter { pointSet.contains(it) }
            while(!pNext.isEmpty()) {
                pointSet.removeAll(pNext)
                pNext=pNext.flatMap { it.pointsArround().filter { pointSet.contains(it) } }
            }
        }
        println("groupcount: $count")

    }

    fun countBits(hex: Char): Int {
        return printBits(hex).map { if('#'==it) 1 else 0 }.sum()
    }

    fun printBits(hex: Char): String {
        return when(hex) {
            '0' -> "...."
            '1' -> "...#"
            '2' -> "..#."
            '3' -> "..##"
            '4' -> ".#.."
            '5' -> ".#.#"
            '6' -> ".##."
            '7' -> ".###"
            '8' -> "#..."
            '9' -> "#..#"
            'a' -> "#.#."
            'b' -> "#.##"
            'c' -> "##.."
            'd' -> "##.#"
            'e' -> "###."
            'f' -> "####"
            else -> throw RuntimeException("bad hex: "+hex)
        }
    }

    fun knotHashAsHex(str: String, list: MutableList<Int>): String {
        var input=par.lineAsAscii(str)
        input.addAll(listOf(17, 31, 73, 47, 23))

        var currentPos=0
        var skipSize=0

        for(r in 1..64) {
            for (inIdx in input.indices) {
                val inLen = input[inIdx]
                replace(list, currentPos, selectSublist(inLen, currentPos, list))
                currentPos = (currentPos + inLen + skipSize) % list.size
                skipSize++
            }
        }

        // 16 numbers
        val denseHash=sparse2dense(list)
        val res=denseHash.map { it.toString(16) }
                .map { if(it.length<2) "0"+it else it }
                .joinToString("")

        return res
    }

    private fun sparse2dense(list: MutableList<Int>): MutableList<Int> {
        var subIdx=0
        val denseHash= mutableListOf<Int>()
        while(subIdx<list.size) {
            var h=list[subIdx]
            for(i in subIdx+1..subIdx+15)
                h = h xor list[i]
            denseHash.add(h)
            subIdx+=16
        }
        return denseHash
    }

    private fun replace(list: MutableList<Int>, currentPos: Int, sublist: MutableList<Int>) {
        for(i in sublist.indices)
            list[(currentPos+i)%list.size]=sublist[i]
    }

    private fun selectSublist(inLen: Int, pCurrentPos: Int, list: MutableList<Int>): MutableList<Int> {
        val ret= mutableListOf<Int>()
        var currentPos=pCurrentPos
        for(i in 1..inLen) {
            ret.add(list[currentPos])
            currentPos=(currentPos+1)%list.size
        }
        ret.reverse()
        return ret
    }
}