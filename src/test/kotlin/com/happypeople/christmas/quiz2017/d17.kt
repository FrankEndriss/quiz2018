package com.happypeople.christmas.quiz2017

import org.apache.commons.collections.list.TreeList
import org.junit.Test

class d17 {
    @Test
    fun d17_star1() {
        val buf = mutableListOf(0)
        var pos = 0
        var steps = 380

        for (i in 1..2017) {
            pos += steps + 1
            pos %= buf.size
            buf.add(pos, i)
        }
        println("result=${buf[pos + 1]}")
    }

    @Test
    fun d17_star2() {
        var pos = 0
        val steps = 380
        // apache.commons.collections
        val tree = TreeList()
        tree.add(0)

        var rightOf0 = -1
        var idx = 0
        for (i in 1..50000000) {
            idx += steps
            idx = idx % tree.size
            // might be faster search afterwards
            if (tree.get(idx) == 0) {
                rightOf0 = i
            }
            idx = idx + 1
            idx = idx % tree.size
            tree.add(idx, i)
        }

        println("rightOf0: ${rightOf0}")
    }

}