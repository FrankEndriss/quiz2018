package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Assert
import org.junit.Test
import java.lang.RuntimeException

class d18 {
    data class Process(val id:Int,
                  val inQ: MutableList<Long>,
                  val outQ: MutableList<Long>,
                  val regs:MutableMap<Char, Long> = mutableMapOf(),
                  var pc:Int =0,
                  var waiting:Boolean=false,
                  var sndCount:Int =0) {
        init {
            for (c in 'a'..'z') {
                regs[c] = 0
            }
            regs['p']=id.toLong()
        }
    }

    var fini = false

    @Test
    fun d18_star2() {
        val par = SimpleParser()
        val prog = par.resAsLines("/2017/in18.txt").map {
            par.lineAsWords(it)
        }

        val q0= mutableListOf<Long>()
        val q1= mutableListOf<Long>()
        val procs=listOf(
                Process(0, q1, q0),
                Process(1, q0, q1)
        )

        fini = false
        var procIdx = 0
        do {
            val process=procs[procIdx%2]
            exec(prog[process.pc], process)
            if(process.waiting) { // taskswitch
                val otherProcess=procs[(procIdx+1)%2]
                if(otherProcess.waiting && process.inQ.isEmpty() && otherProcess.inQ.isEmpty()) {    // both are waiting, finish
                    fini = true
                } else {
                    procIdx++
                }
            }
        } while (!fini)

        println("sndCount ${procs[1].sndCount}")
    }

    var freq = -1L
    private fun exec(w: List<String>, process:Process) {
        // println("command: $w p=$process")
        val cmd = w[0]
        val w1 = w[1]
        val w2 = if (w.size > 2) w[2] else ""
        when (cmd) {
            "snd" -> snd(w1, process)
            "set" -> process.regs[c(w1)] = vOr(w2, process.regs)
            "add" -> process.regs[c(w1)] = process.regs[c(w1)]!! + vOr(w2, process.regs)
            "mul" -> process.regs[c(w1)] = process.regs[c(w1)]!! * vOr(w2, process.regs)
            "mod" -> process.regs[c(w1)] = process.regs[c(w1)]!! % vOr(w2, process.regs)
            "rcv" -> rcv(w1, process)
            "jgz" -> if (vOr(w1, process.regs) > 0) process.pc+=(vOr(w2, process.regs).toInt()-1)
            else -> throw RuntimeException("bad cmd $cmd")
        }
        process.pc++
    }

    private fun snd(w:String, p:Process) {
        p.sndCount++
        p.outQ.add(vOr(w, p.regs))
    }

    private fun rcv(w:String, process:Process) {
        if(process.inQ.size==0) {
            process.waiting=true
            process.pc--
        } else {
            process.waiting=false
            process.regs[c(w)]=process.inQ.removeAt(0)
        }
    }

    fun vOr(s: String, r: MutableMap<Char, Long>): Long {
        if (s[0].isLetter()) // register
            return r[c(s)]!!
        else
            return s.toLong()
    }

    fun c(s: String): Char {
        Assert.assertEquals("only valid on single char String", 1, s.length)
        return s.toCharArray()[0]
    }
}