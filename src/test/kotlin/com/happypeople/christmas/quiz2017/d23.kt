package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Assert
import org.junit.Test
import java.lang.RuntimeException

class d23 {

    data class Process(//val id:Int,
                       //val inQ: MutableList<Long>,
                       //val outQ: MutableList<Long>,
                       val regs:MutableMap<Char, Long> = mutableMapOf(),
                       var pc:Int =0
                       //var waiting:Boolean=false,
                       //var sndCount:Int =0
    ) {
        init {
            for (c in 'a'..'h') {
                regs[c] = 0
            }
            // regs['p']=id.toLong()
        }
    }

    @Test
    fun d23_star2() {
        val prog=parse()
        val proc=Process()

        // var mulC=0
        proc.regs['a']=1
        var step=0L
        do {
            step++
            if(proc.pc==28) {
                println("$step ${proc.pc}: ${proc.regs}")
            }
            if(proc.pc==16) {   // shorten loop here. Better use C code, its aprox 200x faster
                if (proc.regs['d']!! * proc.regs['e']!! > proc.regs['b']!!)
                    proc.regs['e'] = proc.regs['b']!! - 1;
            }

            exec(prog[proc.pc], proc)
        } while (proc.pc>=0 && proc.pc<prog.size)
        println("after ${proc.pc}: g=${proc.regs['g']} b=${proc.regs['b']} d=${proc.regs['d']} e=${proc.regs['e']} h=${proc.regs['h']}")
        println("result: h=${proc.regs['h']}")
    }

    private fun exec(w: List<String>, proc: Process):Int {
        // println("command: $w p=$process")
        val cmd = w[0]
        val w1 = w[1]
        val w2 = if (w.size > 2) w[2] else ""
        when (cmd) {
            "set" -> proc.regs[c(w1)] = vOr(w2, proc.regs)
            "add" -> proc.regs[c(w1)] = proc.regs[c(w1)]!! + vOr(w2, proc.regs)
            "sub" -> proc.regs[c(w1)] = proc.regs[c(w1)]!! - vOr(w2, proc.regs)
            "mul" -> proc.regs[c(w1)] = proc.regs[c(w1)]!! * vOr(w2, proc.regs)
            "mod" -> proc.regs[c(w1)] = proc.regs[c(w1)]!! % vOr(w2, proc.regs)
            // "jgz" -> if (vOr(w1, proc.regs) > 0) proc.pc+=(vOr(w2, proc.regs).toInt()-1)
            "jnz" -> if (vOr(w1, proc.regs) != 0L) proc.pc+=(vOr(w2, proc.regs).toInt()-1)
            else -> throw RuntimeException("bad cmd $cmd")
        }
        proc.pc++
        return if(cmd=="mul") 1 else 0
    }

    fun vOr(s: String, r: MutableMap<Char, Long>): Long {
        if (s[0].isLetter()) // register
            return r[c(s)]!!
        else
            return s.toLong()
    }

    fun c(s: String): Char {
        Assert.assertEquals("only valid on single char String", 1, s.length)
        return s.toCharArray()[0]
    }

    fun parse():List<MutableList<String>> {
        val par=SimpleParser()
        return par.resAsLines("/2017/in23.txt").mapIndexed { idx, line ->
            println("$idx $line")
            par.lineAsWords(line)
        }
    }
}