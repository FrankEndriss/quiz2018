package com.happypeople.christmas.quiz2017

import org.junit.Test

class d05 {

    @Test
    fun d05Test() {
        var input: String = d01::class.java.getResource("/2017/in05.txt").readText()
        val ret= mutableListOf<Int>()
        for(line in input.split("\n")) {
            if(line!=null && line.length>0)
                ret.add(line.toInt())
        }
        println("list: $ret")

        var count=0
        var inst=0
        while(true) {
            count++
            val x=inst
            inst+=ret[x]
            // println("inst: $inst")
            if(inst<0 || inst>=ret.size)
                break
            if(ret[x]>=3)
                ret[x]=ret[x]-1
            else
                ret[x]=ret[x]+1
        }

        println("steps: $count")
    }
}