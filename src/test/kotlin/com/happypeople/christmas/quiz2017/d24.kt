package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class d24 {
    @Test
    fun d24_star1() {
        val comps=parse("/2017/in24.txt")

        val min=appendMore(0, comps)
        val strenAll=stren(comps)
        val strenMin=stren(min)
        val diff=strenAll-strenMin
        println("strenAll: $strenAll strenMin: $strenMin result: $diff")
    }

    // @return the shortest possible sublist of comps, given
    // start the chain with Pin pins.
    fun appendMore(pins:Int, comps:List<List<Int>>):List<List<Int>> {
        return comps.map {
            if(it[0]==pins) {
                // for some reason cant use comp.minus(it) here
                val lList=comps.toMutableList()
                lList.remove(it)
                appendMore(it[1], lList)
            } else if(it[1]==pins) {
                val lList=comps.toMutableList()
                lList.remove(it)
                appendMore(it[0], lList)
            } else
                comps
        }.sortedWith(compareBy({ it.size }, { stren(it) }))
        .first()
    }

    fun stren(list:List<List<Int>>):Int {
        return list.map { it.sum() }.sum()
    }

    private fun parse(res:String):List<List<Int>> {
        val par=SimpleParser()
        return par.resAsLines(res).map {
            par.lineAsInts(it)
        }
    }
}