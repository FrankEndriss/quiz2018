package com.happypeople.christmas.quiz2017

import org.junit.Assert
import org.junit.Test
import java.util.regex.Pattern

data class Point(val x: Int, val y: Int)
val spiralMap= mutableMapOf<Point, Int>()

class d01 {

    @Test
    fun test1_1() {
        var input: String = d01::class.java.getResource("/2017/in01.txt").readText()
        while(!input.last().isDigit())
            input=input.substring(0, input.length-1)

        var count=0
        var lastC=input.last()
        for(c in input) {
            if(c==lastC)
                count+=(""+c).toInt()
            println("$c -> $count")
            lastC=c
        }
        println("count $count")
    }

    @Test
    fun test1_2() {
        var input: String = d01::class.java.getResource("/2017/in01.txt").readText()
        while(!input.last().isDigit())
            input=input.substring(0, input.length-1)

        println("input.length ${input.length}")

        var idx=0
        var sum=0
        for(c in input) {
            if(c==input[(idx+(input.length/2))%input.length])
                sum+=(""+c).toInt()
            idx++
        }
        println("sum $sum")
    }

    @Test
    fun test2_1() {
        var input: String = d01::class.java.getResource("/2017/in02.txt").readText()
        var sum=0
        for(line in input.split("\n")) {
            if(line.length<10)
                continue
            println(line.split(Pattern.compile("\\s+")).filter() { s -> s.length>0 }.map { s -> s.toInt() }.min())
            sum+=(line.split(Pattern.compile("\\s+")).filter() { s -> s.length>0 && s[0].isDigit() }.map { s -> s.toInt() }.max())!!
            sum-=(line.split(Pattern.compile("\\s+")).filter() { s -> s.length>0 && s[0].isDigit()}.map { s -> s.toInt() }.min())!!
            /*
                    */
        }
        println("sum $sum")
    }


    enum class Dir {
        rechts {
            override fun nextPoint(p: Point): Point {
                return Point(p.x+1, p.y)
            }

            override fun nextDir(p: Point):Dir {
                if(spiralMap.contains(up.nextPoint(p)))
                    return Dir.rechts
                else
                    return Dir.up
            }
        },
        up {
            override fun nextPoint(p: Point): Point {
                return Point(p.x, p.y+1)
            }

            override fun nextDir(p: Point):Dir {
                if(spiralMap.contains(links.nextPoint(p)))
                    return Dir.up
                else
                    return Dir.links
            }
        },
        links {
            override fun nextPoint(p: Point): Point {
                return Point(p.x-1, p.y)
            }

            override fun nextDir(p: Point):Dir {
                if(spiralMap.contains(down.nextPoint(p)))
                    return Dir.links
                else
                    return Dir.down
            }
        },
        down {
            override fun nextPoint(p: Point): Point {
                return Point(p.x, p.y-1)
            }

            override fun nextDir(p: Point):Dir {
                if(spiralMap.contains(rechts.nextPoint(p)))
                    return Dir.down
                else
                    return Dir.rechts
            }
        };

        abstract fun nextDir(p: Point):Dir
        abstract fun nextPoint(p: Point):Point
    }

    // sum of the 8 neigbours
    private fun adjentSum(p: Point): Int {
        return listOf(
                Point(p.x-1, p.y),
                Point(p.x-1, p.y+1),
                Point(p.x, p.y+1),
                Point(p.x+1, p.y+1),
                Point(p.x+1, p.y),
                Point(p.x+1, p.y-1),
                Point(p.x, p.y-1),
                Point(p.x-1, p.y-1)
        ).filter() { p -> spiralMap.contains(p) }.sumBy { p -> spiralMap[p]!! }
    }

    @Test
    fun test3_2() {
        var dir=Dir.rechts
        var point=Point(0, 0)
        spiralMap[point]=1
        point=dir.nextPoint(point)
        dir=dir.nextDir(point)

        while(true) {
            spiralMap[point]=adjentSum(point)
            if(spiralMap[point]!!>289326)
                break
            point=dir.nextPoint(point)
            dir=dir.nextDir(point)
        }

        println("value: ${spiralMap[point]}")
    }

    @Test
    fun test3_1() {
        var dir=Dir.rechts
        var point=Point(0, 0)
        var count=0
        while(true) {
            spiralMap[point]=count
            count++
            if(count==289326)
                break
            point=dir.nextPoint(point)
            dir=dir.nextDir(point)
        }

        val x=Math.abs(point.x)
        val y=Math.abs(point.y)
        val sum=x+y
        println("x=$x y=$y sum=$sum")
    }

    private fun sortLetters(str: String): String {
        return ""+(str.toCharArray().toList().sorted())
    }

    @Test
    fun testSortLetters() {
        val str1="hallo"
        val str2="holla"
        val str1Sorted=sortLetters(str1)
        val str2Sorted=sortLetters(str2)

        println("$str1 : $str1Sorted")
        println("$str2 : $str2Sorted")

        Assert.assertEquals("should be same", str1Sorted, str2Sorted)

    }

    @Test
    fun test4_2() {
        var input: String = d01::class.java.getResource("/2017/in04.txt").readText()
        var va=0
        var nva=0
        for(line in input.split("\n")) {
            if(line==null || line.length<3)
                continue
            val count1=line.split(Pattern.compile("\\s+"))
                    .filter() { s -> s.length>0 }
                    .map() { s -> sortLetters(s) }
                    .size
            val count2=line.split(Pattern.compile("\\s+"))
                    .filter() { s -> s.length>0 }
                    .map() { s -> sortLetters(s) }
                    .toSet().size

            if(count1==count2)
                va++
            else
                nva++
        }
        println("valid=$va nvalid=$nva")
    }

    @Test
    fun test4_1() {
        var input: String = d01::class.java.getResource("/2017/in04.txt").readText()
        var va=0
        var nva=0
        for(line in input.split("\n")) {
            if(line==null || line.length<3)
                continue
            val count1=line.split(Pattern.compile("\\s+")).filter() { s -> s.length>0 }.size
            val count2=line.split(Pattern.compile("\\s+")).filter() { s -> s.length>0 }.toSet().size

            if(count1==count2)
                va++
            else
                nva++
        }
        println("valid=$va nvalid=$nva")
    }

    @Test
    fun test2_2() {
        var input: String = d01::class.java.getResource("/2017/in02.txt").readText()
        var sum=0
        for(line in input.split("\n")) {
            if(line.length<10)
                continue

            val list=line.split(Pattern.compile("\\s+")).filter() { s -> s.length>0 && s[0].isDigit() }.map { s -> s.toInt() }
            var linesum=0
            for(i in list) {
                for(j in list) {
                    if(i==j)
                        continue
                    if(i%j==0 || j%i==0) {
                        linesum += i / j
                        linesum += j / i
                        break
                    }
                }
            }
            sum+=(linesum/2)
        }
        println("sum $sum")
    }

    @Test
    fun charConf() {
        val c='7'
        println("Char: $c "+(""+c))
    }
}