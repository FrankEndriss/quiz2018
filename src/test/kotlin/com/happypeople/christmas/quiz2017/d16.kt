package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Assert
import org.junit.Test

class d16 {
    var progs_init="abcdefghijklmnop"
    var progs=progs_init

    @Test
    fun d16_star1() {
        run()
        println("progs: $progs")
    }

    @Test
    fun d16_star2() {
        val set= mutableSetOf<String>(progs)
        var i=0
        var fini=false
        do {
            run()
            i++
            if(set.contains(progs)) {
                println(i)
                fini = true
            } else
                set.add(progs)
        }while(!fini)

        progs=progs_init
        for(x in 1..(1000000000%i))
            run()
        println("$progs")
    }


    fun run() {
        val par=SimpleParser()
        val str= par.resAsString("/2017/in16.txt")
        val words=str.split(",")
        words.forEach {
            if (it.startsWith("s")) {
                val ints = par.lineAsInts(it)
                // println("ints $ints")
                // println("progs1: $progs")
                progs=progs.substring(progs.length-ints[0])+progs.substring(0, progs.length-ints[0])
                // println("progs2: $progs")
            } else if (it.startsWith("x")) {
                val ints = par.lineAsInts(it)
                val arr=progs.toCharArray()
                val x1=arr[ints[0]]
                arr[ints[0]]=arr[ints[1]]
                arr[ints[1]]=x1
                progs=String(arr)
            } else if (it.startsWith("p")) {
                val c0=it[1]
                val c1=it[3]
                val idx0=progs.indexOf(c0)
                val idx1=progs.indexOf(c1)
                val arr=progs.toCharArray()
                arr[idx0]=c1
                arr[idx1]=c0
                progs=String(arr)

            } else
                println("ignored: "+it)
        }
    }

    @Test
    fun arrToString() {
        val str="123"
        val arr=str.toCharArray()
        val str2=String(arr)
        Assert.assertEquals(str, str2)
    }
}