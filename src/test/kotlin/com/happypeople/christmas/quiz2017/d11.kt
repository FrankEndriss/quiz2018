package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class d11 {
    val par=SimpleParser()

    @Test
    fun d11_star1() {
        val inp=par.resAsString("/2017/in11.txt").trim().split(",")
        val data=inp.map { str -> when(str) {
            "n" -> Pair(0, -2)
            "nw" -> Pair(-1, -1)
            "ne" -> Pair(1, -1)
            "sw" -> Pair(-1, 1)
            "s" -> Pair(0, 2)
            "se" -> Pair (1, 1)
            else -> throw RuntimeException("bad input: >>$str<<")
        } }

        var x=0
        var y=0
        val abst= mutableListOf<Int>()
        for(p in data) {
            x+=p.first
            y+=p.second
            abst.add(abstand(x, y))
        }
        println("abstand= ${abstand(x, y)}")
        val maxAbst=abst.max()
        println("max abstand= ${maxAbst}")
    }

    /** @return Anzahl steps von 0, 0 */
    fun abstand(x: Int, y: Int):Int {
        val ax=Math.abs(x)
        val ay=Math.abs(y)
        if(ax>ay)
            return ax

        val diff=ay-ax
        return ax+(diff/2)
    }
}