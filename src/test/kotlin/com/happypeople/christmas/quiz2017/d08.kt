package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class d08 {

    @Test
    fun d08_star1() {
        val par=SimpleParser()
        val regs=mutableMapOf<String, Int>()
        var highest=0
        for(line in par.resAsLines("/2017/in08.txt")) {
            val words=par.lineAsWords(line)
            val old=regs.getOrDefault(words[0], 0)
            val crement=words[2].toInt()
            val condRegVal=regs.getOrDefault(words[4], 0)
            val condVal=words[6].toInt()
            val condOp=words[5]

            // if cond matches
            val condMatches=
            if(condOp==">=")
                condRegVal>=condVal
            else if(condOp==">")
                condRegVal>condVal
            else if(condOp=="<=")
                condRegVal<=condVal
            else if(condOp=="<")
                condRegVal<condVal
            else if(condOp=="==")
                condRegVal==condVal
            else if(condOp=="!=")
                condRegVal!=condVal
            else
                throw RuntimeException("unknown condOp $condOp")

            if(condMatches) {
                if(words[1]=="inc")
                    regs[words[0]]=old+crement
                else
                    regs[words[0]]=old-crement

                highest=Math.max(highest, regs[words[0]]!!)
            }
        }
        // result
        val largest=regs.values.max()
        println("largest $largest highest $highest")

    }
}