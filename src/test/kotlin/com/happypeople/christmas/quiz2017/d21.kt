package com.happypeople.christmas.quiz2017


import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test
import kotlin.math.max

typealias Pic = List<List<Char>>

// `1234567890-=
// ~!@#$%^&*()_+

// qwertyuiop[]
// QWERTYUIOP{}

// asdfghjkl;'\
// ASDFGHJKKL:"|

// \zxcvbnm,./
// |ZXCVBNM<>?
class d21 {
    val start = listOf(
            listOf('.', '#', '.'),
            listOf('.', '.', '#'),
            listOf('#', '#', '#')
    )

    var rules = listOf<Rule>()

    @Test
    fun d21_both_stars() {
        rules = parseRules()

        for(star in listOf(5, 18)) {
            var p = start
            for (i in 1..star) {
                p = oneIteration(p)
                //println("iter $i fini")
                print(".")
            }
            println()
            //viz(p)

            val c = p.map {
                it.count { it == '#' }
            }.sum()

            println("on after $star: $c")
        }

    }

    private fun oneIteration(p: Pic): Pic {
        val s = p.size
        val splitted = split(p, if (s % 2 == 0) s / 2 else s / 3)
        val x = splitted.map {
            it.map {
                applyRule(it)
            }
        }
        return flatten(x)
    }

    private fun applyRule(pic: Pic): Pic {
        // use asSequence() to speed things up
        return rules.asSequence().map {
            if (it.matches(pic))
                it.to
            else
                null
        }.filterNotNull().first()
    }

    // split a List<List<Char>> (a Pic) into parts of it, a List<List<Pic>> of size numPieces by numPieces
    private fun split(t: Pic, numPieces: Int): List<List<Pic>> {
        val s2 = t.size / numPieces
        return (0..numPieces - 1).map { y ->
            (0..numPieces - 1).map {
                cut(t, s2 * it, y * s2, s2)
            }
        }
    }

    // combines a NbyN Pic into one single Pic (flatten twice), opposite of split
    private fun flatten(t: List<List<Pic>>): Pic {
        val a = mutableListOf<List<Char>>()
        val outerS = t.size         // any number
        val innerS = t[0][0].size   // 3 or 4
        for (y in 0..outerS - 1) {
            for (innerY in 0..innerS - 1) {
                val line = mutableListOf<Char>()
                for (x in 0..outerS - 1) {
                    line.addAll(t[y][x][innerY])
                }
                a.add(line)
            }
        }
        return a
    }

    // cuts a piece out of t, starting at x/y, size s in both dirs
    private fun cut(t: Pic, x: Int, y: Int, s: Int): Pic {
        return t.mapIndexed { index, list ->
            if (index >= y && index < y + s)
                list
            else
                null
        }.filterNotNull()
                .map { list ->
                    list.slice(x..x + s - 1)
                }
    }

    private fun flipV(t: Pic): Pic {
        if (t.size == 3)
            return rot3(rot3(rot3(flipH(rot3(t)))))
        else
            return rot2(rot2(rot2(flipH(rot2(t)))))
    }

    private fun flipH(t: Pic): Pic {
        return t.mapIndexed { idx, line ->
            t[t.size - idx - 1]
        }
    }

    fun rot2(p: Pic): Pic {
        return listOf(
                listOf(p[0][1], p[1][1]),
                listOf(p[0][0], p[1][0])
        )
    }

    fun rot3(p: Pic): Pic {
        return listOf(
                listOf(p[0][2], p[1][2], p[2][2]),
                listOf(p[0][1], p[1][1], p[2][1]),
                listOf(p[0][0], p[1][0], p[2][0])
        )
    }

    private fun viz(pic: Pic) {
        println()
        pic.forEach { line ->
            line.forEach { print(it) }
            println()
        }
    }

    inner class Rule(val data: List<Pic>) {
        val to = data[1]
        val from = data[0]
        val fromPerms: Set<Pic>

        init {
            val s = from.size
            fromPerms =
                    listOf(from, flipH(from)).flatMap {
                        listOf(it, flipV(it)).flatMap {
                            val r1 = if (s == 2) rot2(it) else rot3(it)
                            val r2 = if (s == 2) rot2(r1) else rot3(r1)
                            val r3 = if (s == 2) rot2(r2) else rot3(r2)
                            listOf(from, r1, r2, r3)
                        }
                    }.toSet()
        }

        fun matches(p: Pic): Boolean {
            return fromPerms.contains(p)
        }
    }

    private fun parseRules(): List<Rule> {
        val par = SimpleParser()
        return par.resAsLines("/2017/in21.txt").map { line ->
            Rule(
                    line.trim().split(" => ").map { picstring ->
                        picstring.trim().split("/").map { picline ->
                            picline.toList()
                        }
                    }
            )
        }
    }
}