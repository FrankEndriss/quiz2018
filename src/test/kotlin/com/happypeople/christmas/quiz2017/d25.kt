package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class d25 {
    val states=arrayOf(
            // write, move, nextState
            arrayOf(arrayOf(1, 1, 1), arrayOf( 0, 1, 2)),
            arrayOf(arrayOf(0, -1, 0), arrayOf(0, 1, 3)),
            arrayOf(arrayOf(1, 1, 3), arrayOf(1, 1, 0)),
            arrayOf(arrayOf(1, -1, 4), arrayOf(0, -1, 3)),
            arrayOf(arrayOf(1, 1, 5), arrayOf(1, -1, 1)),
            arrayOf(arrayOf(1, 1, 0), arrayOf(1, 1, 4))
    )

    @Test
    fun d25_star1() {
        val tape=Array(100000, { 0 })
        var steps=12399302

        var idx=tape.size/2
        var state=states[0]
        while(steps-->0) {
            val a= state[tape[idx]]
            tape[idx]=a[0]
            idx+=a[1]
            state=states[a[2]]
        }
        val count=tape.count { it==1 }
        println("count $count")
    }

    fun parse(res:String) {
        val par=SimpleParser()
        par.resAsLines(res)
    }
}