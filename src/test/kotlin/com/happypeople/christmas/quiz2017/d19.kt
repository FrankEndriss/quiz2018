package com.happypeople.christmas.quiz2017

import com.happypeople.christmas.quiz2018.SimpleParser
import org.junit.Test

class d19 {
    data class P(val x: Int, val y: Int) {
        fun add(p: P): P {
            return P(x + p.x, y + p.y)
        }

        fun onTrack(x: Map<P, Char>): Boolean {
            val c = x[this]
            return c != null && (c.isLetter() || "|-+".contains(c))
        }

        fun opposite(): P {
            return P(-x, -y)
        }
    }

    val dirs = listOf(
            P(0, 1), // down
            P(0, -1),
            P(-1, 0),
            P(1, 0)
    )

    @Test
    fun d19_star1() {
        val x = parse("/2017/in19.txt")
        var p = x.filter { it.key.y == 0 && it.value == '|' }.map { it.key }.first()
        var dir = dirs[0]
        val result = String(seqOfLetters(p, dir, x))
        println(result)
    }

    @Test
    fun d19_star2() {
        val x = parse("/2017/in19.txt")
        var p = x.filter { it.key.y == 0 && it.value == '|' }.map { it.key }.first()
        var dir = dirs[0]
        val result = seqOfPositions(p, dir, x).count() + 1
        println(result)
    }

    fun seqOfLetters(p: P, dir: P, x: Map<P, Char>): CharArray {
        return seqOfPositions(p, dir, x)
                .map { x[it]!! }
                .filter { it.isLetter() }
                .toList()
                .toCharArray()
    }

    private fun seqOfPositions(pp: P, pDir: P, x: Map<P, Char>): Sequence<P> {
        var p = pp      // current position
        var dir = pDir  // current direction

        return generateSequence {
            val ps = p.add(dir)
            if (ps.onTrack(x)) {
                p = ps
                ps
            } else {
                // note that this does not check for directions of '-' and '|'
                // which does not work if track has lines with no space in between
                val lDir = dirs.filter { it != dir.opposite() && it != dir }
                        .filter { p.add(it).onTrack(x) }
                        .firstOrNull()
                if (lDir != null) {
                    dir=lDir
                    p = lDir.add(p)
                    p
                } else
                    null
            }
        }
    }

    fun parse(res: String): Map<P, Char> {
        val par = SimpleParser()
        return par.resAsLines(res).mapIndexed { y, s ->
            s.mapIndexed { x, c ->
                P(x, y) to c
            }
        }.flatten().toMap()
    }

}