
#include <stdio.h>

int run() {
	int a=1;
	int b=0;
	int c=0;
	int d=0;
	int e=0;
	int f=0;
	int g=0;
	int h=0;

// 00 set b 79             b=79
	b=79;
// 01 set c b              c=79
	c=b;
// 02 jnz a 2              if(a) GOTO 04 else GOTO 05
	if(a)
		goto g04;
// 03 jnz 1 5
	goto g05;
// 04 mul b 100            b*=100
g04:
	b*=100;
// 05 sub b -100000        b-=100000
g05:
	b+=100000;
//06 set c b              c=b
	c=b;
// 07 sub c -17000         c-=17000
	c+=17000;
////////////////////////////////////////////////////////////////////////////end init
printf("07 init, a=%d b=%d c=%d d=%d e=%d f=%d g=%d h=%d\n", a, b, c, d, e, f, g, h);
			//// b==107900 ; c=124900
g08:
// 08 set f 1           // loopG08 | do { f=1; d=2; loopG10 ; if(f==0) h++; b+=17; } while(b!=c)
// 			// ...for(b=107900; b<124900; b+=17)... 
// printf("set f=1, a=%d b=%d c=%d d=%d e=%d f=%d g=%d h=%d\n", a, b, c, d, e, f, g, h);
	f=1;
// 09 set d 2              d=2
	d=2;

			// ...for(d=2; d!=b; d++)
g10:			// loopG10 | do { e=2 ; loopG11 ; d++; } while(d!=b)
// 10 set e 2
	e=2;

			// ...for(e=2; e!=b; e++) if(d*e==b) f=0; 
g11:			// loopG11 | do { if(d*e-b==0) f=0 ; e++  } while(e!=b) 
// 11 set g d
	g=d;
// 12 mul g e
	g*=e;
// 13 sub g b
	g-=b;
// 14 jnz g 2		// if(g==0) f=0
	if(g!=0)
		goto g16;
// 15 set f 0
// printf("set f=0, a=%d b=%d c=%d d=%d e=%d f=%d g=%d h=%d\n", a, b, c, d, e, f, g, h);
	f=0;
g16:
if(d*e>b)   // shorten the loop here!!!
	e=b-1;
// 16 sub e -1		// e++ ; g=e-b
	e++;
// 17 set g e
	g=e;
// 18 sub g b
	g-=b;
// 19 jnz g -8
	if(g!=0)
		goto g11;		// loopG11
// 20 sub d -1
	d++;
// 21 set g d
	g=d;
// 22 sub g b
	g-=b;
// 23 jnz g -13
// printf("23, g=%d h=%d\n", g, h);
	if(g!=0)
		goto g10;		// loopG10
// 24 jnz f 2
	if(f!=0)
		goto g26;
// 25 sub h -1
// printf("h++, a=%d b=%d c=%d d=%d e=%d f=%d g=%d h=%d\n", a, b, c, d, e, f, g, h);
	h++;
g26:
// 26 set g b
	g=b;
// 27 sub g c
	g-=c;

// 28 jnz g 2
// printf("28, a=%d b=%d c=%d d=%d e=%d f=%d g=%d h=%d\n", a, b, c, d, e, f, g, h);
	if(g)
		goto g30;
// 29 jnz 1 3
	return h;
g30:
// 30 sub b -17
	b+=17;

// 31 jnz 1 -23
	goto g08;		// loopG8
}

int main(int argc, char** argv) {
	int res=run();
	printf("res: %d\n", res);
}

