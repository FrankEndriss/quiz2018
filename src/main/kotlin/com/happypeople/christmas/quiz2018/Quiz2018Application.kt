package com.happypeople.christmas.quiz2018

import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import java.util.Arrays

@SpringBootApplication
open class Quiz2018Application {
	private val log = LoggerFactory.getLogger(Quiz2018Application::class.java)
	
	@Bean
	public open fun init(ctx: ApplicationContext) = CommandLineRunner {
		log.info("Let's inspect the beans provided by Spring Boot:");

		var beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for (beanName in beanNames) {
			log.info(beanName);
		}
	}
}

fun main(args: Array<String>) {
	runApplication<Quiz2018Application>(*args)
}
