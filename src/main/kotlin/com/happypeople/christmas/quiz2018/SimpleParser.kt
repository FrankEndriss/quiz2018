package com.happypeople.christmas.quiz2018

import java.util.regex.Pattern

/** Simple parser oneliners. */
class SimpleParser {

    fun resAsString(resname: String):String {
        return SimpleParser::class.java.getResource(resname).readText()
    }

    fun resAsLines(resname: String): MutableList<String> {
        return resAsString(resname)
                .split("\n")
                .filter { it!=null && it.length>0 }
                .toMutableList()
    }

    fun lineAsWords(line: String): MutableList<String> {
        val pWord= Pattern.compile("\\S+")
        val m=pWord.matcher(line)

        return generateSequence {
            if(m.find())
                m.group()
            else
                null
        }.filter { it.length>0 }.toMutableList()
    }

    fun lineAsInts(line: String): MutableList<Int> {
        val pDecimal= Pattern.compile("-?\\d+")
        val m=pDecimal.matcher(line)
        return generateSequence {
            if(m.find())
                m.group().toInt()
            else
                null
        }.toMutableList()
    }

    /** @return A List of the ascii codes of the bytes in line */
    fun lineAsAscii(line: String): MutableList<Int> {
        return line
                .map { c -> c.toInt() }
                .toMutableList()
    }

    fun resAsInts2D(resname: String): MutableList<MutableList<Int>> {
        return resAsLines(resname).map { lineAsInts(it) }.toMutableList()
    }
}